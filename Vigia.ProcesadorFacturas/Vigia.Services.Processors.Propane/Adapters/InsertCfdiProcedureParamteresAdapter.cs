﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Vigia.Common.Database;
using Vigia.Services.Processors.Propane.Models;

namespace Vigia.Services.Processors.Propane.Adapters
{
    internal class InsertCfdiProcedureParamteresAdapter : IProcedureParametersAdapter<Cfdi>
    {
        private readonly Guid session;

        public InsertCfdiProcedureParamteresAdapter(Guid session)
        {
            this.session = session;
        }

        public object[] Adapt(Cfdi toAdapt)
        {
            IList<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@sesion", session));
            parameters.Add(new SqlParameter("@Comprobanteserie", toAdapt.Comprobante.Serie));
            parameters.Add(new SqlParameter("@Comprobantefolio ", toAdapt.Comprobante.Folio));
            parameters.Add(new SqlParameter("@Comprobantefecha ", toAdapt.Comprobante.Fecha));
            parameters.Add(new SqlParameter("@Comprobantesello ", toAdapt.Comprobante.Sello));
            parameters.Add(new SqlParameter("@Comprobanteformapago ", toAdapt.Comprobante.FormaPago));
            parameters.Add(new SqlParameter("@Comprobantenocertificado ", toAdapt.Comprobante.NumCertificado));
            parameters.Add(new SqlParameter("@Comprobantecertificado ", toAdapt.Comprobante.Certificado));
            parameters.Add(new SqlParameter("@Comprobantecondicionesdepago ", toAdapt.Comprobante.CondicionesDePago));
            parameters.Add(new SqlParameter("@Comprobantesubtotal", toAdapt.Comprobante.SubTotal));
            parameters.Add(new SqlParameter("@Comprobantedescuento", toAdapt.Comprobante.Descuento));
            parameters.Add(new SqlParameter("@Comprobantetotal", toAdapt.Comprobante.Total));
            parameters.Add(new SqlParameter("@ComprobantemetodoDePago", toAdapt.Comprobante.MetodoDePago));
            parameters.Add(new SqlParameter("@ComprobantetipoDeComprobante", toAdapt.Comprobante.TipoDeComprobante));
            parameters.Add(new SqlParameter("@ComprobanteLugarExpedicion", toAdapt.Comprobante.LugarExpedicion));
            parameters.Add(new SqlParameter("@Emisorrfc", toAdapt.Emisor.Rfc));
            parameters.Add(new SqlParameter("@Emisornombre", toAdapt.Emisor.Nombre));
            parameters.Add(new SqlParameter("@DomicilioFiscalcalle", toAdapt.DomicilioFiscal.Calle));
            parameters.Add(new SqlParameter("@DomicilioFiscalnoExterior", toAdapt.DomicilioFiscal.NoExterior));
            parameters.Add(new SqlParameter("@DomicilioFiscalcolonia", toAdapt.DomicilioFiscal.Colonia));
            parameters.Add(new SqlParameter("@DomicilioFiscalmunicipio", toAdapt.DomicilioFiscal.Municipio));
            parameters.Add(new SqlParameter("@DomicilioFiscalestado", toAdapt.DomicilioFiscal.Estado));
            parameters.Add(new SqlParameter("@DomicilioFiscalpais", toAdapt.DomicilioFiscal.Pais));
            parameters.Add(new SqlParameter("@DomicilioFiscalcodigoPostal", toAdapt.DomicilioFiscal.CodigoPostal));
            parameters.Add(new SqlParameter("@ExpedidoEncalle", toAdapt.ExpedidoEn.Calle));
            parameters.Add(new SqlParameter("@ExpedidoEnnoExterior", toAdapt.ExpedidoEn.NoExterior));
            parameters.Add(new SqlParameter("@ExpedidoEncolonia", toAdapt.ExpedidoEn.Colonia));
            parameters.Add(new SqlParameter("@ExpedidoEnmunicipio", toAdapt.ExpedidoEn.Municipio));
            parameters.Add(new SqlParameter("@ExpedidoEnestado", toAdapt.ExpedidoEn.Estado));
            parameters.Add(new SqlParameter("@ExpedidoEnpais", toAdapt.ExpedidoEn.Pais));
            parameters.Add(new SqlParameter("@ExpedidoEncodigoPostal", toAdapt.ExpedidoEn.CodigoPostal));
            parameters.Add(new SqlParameter("@RegimenFiscalRegimen", toAdapt.RegimenFiscal.Regimen));
            parameters.Add(new SqlParameter("@Receptorrfc", toAdapt.Receptor.Rfc));
            parameters.Add(new SqlParameter("@Receptornombre", toAdapt.Receptor.Nombre));
            parameters.Add(new SqlParameter("@Domiciliocalle", toAdapt.Domicilio.Calle));
            parameters.Add(new SqlParameter("@Domiciliomunicipio", toAdapt.Domicilio.Municipio));
            parameters.Add(new SqlParameter("@Domicilioestado ", toAdapt.Domicilio.Estado));
            parameters.Add(new SqlParameter("@Domiciliopais", toAdapt.Domicilio.Pais));
            parameters.Add(new SqlParameter("@DomiciliocodigoPostal", toAdapt.Domicilio.CodigoPostal));
            parameters.Add(new SqlParameter("@ImpuestostotalImpuestosTrasladados", toAdapt.Impuestos.ImpuestosTrasladados));
            parameters.Add(new SqlParameter("@TimbreFiscalDigitalUUID", toAdapt.TimbreFiscalDigital.Uuid));
            parameters.Add(new SqlParameter("@TimbreFiscalDigitalFechaTimbrado", toAdapt.TimbreFiscalDigital.FechaTimbrado));
            parameters.Add(new SqlParameter("@TimbreFiscalDigitalselloCFD", toAdapt.TimbreFiscalDigital.SelloCfd));
            parameters.Add(new SqlParameter("@TimbreFiscalDigitalnoCertificadoSAT", toAdapt.TimbreFiscalDigital.NoCertificadoSat));
            parameters.Add(new SqlParameter("@TimbreFiscalDigitalselloSAT", toAdapt.TimbreFiscalDigital.SelloSat));
            parameters.Add(new SqlParameter("@DatosCadenaOriginal", toAdapt.Datos.CadenaOriginal));

            string mensaje = "";
            foreach (var m in toAdapt.Mensaje)
                mensaje += m.Msj + "\n";
            parameters.Add(new SqlParameter("@Mensaje", mensaje));

            SqlParameter cfdiId = new SqlParameter();
            cfdiId.ParameterName = "@factura_id";
            cfdiId.Direction = ParameterDirection.Output;
            cfdiId.DbType = DbType.Int32;
            parameters.Add(cfdiId);

            return parameters.ToArray();
        }
    }
}
