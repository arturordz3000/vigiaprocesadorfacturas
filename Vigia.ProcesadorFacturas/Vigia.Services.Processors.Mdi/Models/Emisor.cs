﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class Emisor
    {
        [MapFieldFromDataSet(field: "RegimenFiscal", defaultValue: "")]
        public string RegimenFiscal { get; set; }

        [MapFieldFromDataSet(field: "rfc", defaultValue: "")]
        public string Rfc { get; set; }

        [MapFieldFromDataSet(field: "nombre", defaultValue: "")]
        public string Nombre { get; set; }
    }
}
