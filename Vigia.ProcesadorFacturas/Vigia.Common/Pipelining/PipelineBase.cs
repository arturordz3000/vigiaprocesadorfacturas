﻿using System;
using System.Collections.Generic;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;

namespace Vigia.Common.Pipelining
{
    public abstract class PipelineBase : IPipeline
    {
        protected IList<IComponent> Components { get; private set; }
        protected ILog logger;
        protected readonly ILogFactory logFactory;

        public PipelineBase(ILogFactory logFactory, params object[] services)
        {
            Components = new List<IComponent>();
            logger = logFactory.Create(typeof(PipelineBase));
            this.logFactory = logFactory;

            InitializeServices(services);
            Build();
        }

        public abstract void InitializeServices(object[] services);

        public abstract void Build();

        public virtual void Execute()
        {
            // Create a new session id that will distinguish the current pipeline process.
            Guid sessionId = Guid.NewGuid();
            object currentInput = null;

            try
            {
                foreach (IComponent c in Components)
                    currentInput = c.Execute(sessionId, currentInput);
            }
            catch (Exception ex)
            {
                logger.Error($"Interrumpiendo ejecución de sesión ${sessionId} debido al siguiente error: {ex.Message}");
            }
        }
    }
}
