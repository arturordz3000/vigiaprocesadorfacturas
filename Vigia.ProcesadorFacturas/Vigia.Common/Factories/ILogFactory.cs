﻿using System;
using Vigia.Common.Logging;

namespace Vigia.Common.Factories
{
    public interface ILogFactory
    {
        ILog Create(Type type);
    }
}
