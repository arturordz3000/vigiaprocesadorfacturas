﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vigia.Common.Database;
using Vigia.ProcesadorFacturas.Service.Configuration;
using Vigia.Services.Common.Configuration;

namespace Vigia.ProcesadorFacturas.Service.Services
{
    internal class ProcessorsConfigService : IProcessorsConfigService
    {
        IDatabase database;

        public ProcessorsConfigService(IDatabase database)
        {
            this.database = database;
        }

        public ProcessorConfiguration[] GetProcessorsConfiguration()
        {
            return database.ExecuteQuery<ProcessorConfiguration[]>($"SELECT * FROM {AppSettings.ProcessorsConfigurationTableName} WHERE ZI_ENVIOFA = 'S'");
        }
    }
}
