﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Vigia.Common.Database;

namespace Vigia.Services.Processors.Propane.Adapters
{
    internal class InsertCfdiDetailsProcedureParametersAdapter : IProcedureParametersAdapter<DataRow>
    {
        internal enum Case
        {
            Version3,
            AnyVersion
        }

        private readonly Guid session;
        private readonly int cfdiId;
        private readonly Case adaptingCase;

        public InsertCfdiDetailsProcedureParametersAdapter(Guid session, int cfdiId, Case adaptingCase)
        {
            this.session = session;
            this.cfdiId = cfdiId;
            this.adaptingCase = adaptingCase;
        }

        public object[] Adapt(DataRow toAdapt)
        {
            switch (adaptingCase)
            {
                case Case.Version3:
                    return AdaptOnVersion3(toAdapt);
                case Case.AnyVersion:
                    return AdaptOnAnyVersion(toAdapt);
            }

            throw new Exception("Unexisting case");
        }

        private object[] AdaptOnVersion3(DataRow detailDataRow)
        {
            IList<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@factura_id", cfdiId));
            parameters.Add(new SqlParameter("@sesion", session));
            parameters.Add(new SqlParameter("@cantidad", decimal.Parse(detailDataRow[2].ToString())));
            parameters.Add(new SqlParameter("@descripcion", detailDataRow[5]));
            parameters.Add(new SqlParameter("@unidad", detailDataRow[4]));
            parameters.Add(new SqlParameter("@valorUnitario", decimal.Parse(detailDataRow[6].ToString())));
            parameters.Add(new SqlParameter("@importe", decimal.Parse(detailDataRow[7].ToString())));


            return parameters.ToArray();
        }

        private object[] AdaptOnAnyVersion(DataRow detailDataRow)
        {
            IList<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@factura_id", cfdiId));
            parameters.Add(new SqlParameter("@sesion", session));
            parameters.Add(new SqlParameter("@cantidad", decimal.Parse(detailDataRow[0].ToString())));
            parameters.Add(new SqlParameter("@descripcion", detailDataRow[1]));
            parameters.Add(new SqlParameter("@unidad", detailDataRow[2]));
            parameters.Add(new SqlParameter("@valorUnitario", decimal.Parse(detailDataRow[3].ToString())));
            parameters.Add(new SqlParameter("@importe", decimal.Parse(detailDataRow[4].ToString())));


            return parameters.ToArray();
        }
    }
}
