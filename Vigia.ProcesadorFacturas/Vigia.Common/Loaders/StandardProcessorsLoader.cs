﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Vigia.Common.Configuration;
using Vigia.Common.Extensions;
using Vigia.Common.Processing;

namespace Vigia.Common.Loaders
{
    public class StandardProcessorsLoader : ILoader<IProcessor[]>
    {
        private readonly Assembly[] assemblies;

        public StandardProcessorsLoader()
        {
            string[] assembliesPaths = ApplicationConfiguration.GetInstance().ProcessorsBasePath.Split(';');
            assemblies = LoadAssembliesFromPaths(assembliesPaths);
        }

        private Assembly[] LoadAssembliesFromPaths(string[] assembliesPaths)
        {
            IList<Assembly> assembliesList = new List<Assembly>();

            foreach (string path in assembliesPaths)
                assembliesList.Add(Assembly.LoadFile(Path.GetFullPath(path)));

            return assembliesList.ToArray();
        }

        public StandardProcessorsLoader(Assembly[] assemblies)
        {
            this.assemblies = assemblies;
        }

        public IProcessor[] Load()
        {
            IList<IProcessor> processors = new List<IProcessor>();

            foreach (Assembly assembly in assemblies)
                processors.Add(assembly.GetProcessor());

            return processors.ToArray();
        }
    }
}
