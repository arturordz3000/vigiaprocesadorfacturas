﻿using Vigia.Common.Factories;
using Vigia.Common.Mapping;
using Vigia.Common.Pipelining;
using Vigia.Services.Processors.Propane.Components;
using Vigia.Services.Processors.Propane.Services;

namespace Vigia.Services.Processors.Propane.Pipelining
{
    internal class StandardPipeline : PipelineBase
    {
        private IFacturasService facturasService;
        private IDataSetMapper dataSetMapper;
        private IReportsService reportsService;

        public StandardPipeline(ILogFactory logFactory, IFacturasService facturasService, IDataSetMapper dataSetMapper, IReportsService reportsService)
            : base(logFactory, facturasService, dataSetMapper, reportsService) { }

        public override void InitializeServices(object[] services)
        {
            facturasService = (IFacturasService)services[0];
            dataSetMapper = (IDataSetMapper)services[1];
            reportsService = (IReportsService)services[2];
        }

        public override void Build()
        {
            Components.Add(new FacturasFetcherComponent(logFactory, facturasService));
            Components.Add(new FacturaValidationComponent(logFactory));
            Components.Add(new CfdiBuilderComponent(logFactory, dataSetMapper));
            Components.Add(new CfdiInsertionComponent(logFactory, facturasService));
            Components.Add(new CfdiDetailInsertionComponent(logFactory, facturasService));
            Components.Add(new PdfGenerationComponent(logFactory, reportsService));
#if !PRERELEASE
            Components.Add(new MailSenderComponent(logFactory, facturasService));
            Components.Add(new FacturaElectronicaInsertionComponent(logFactory, facturasService));
#endif
        }
    }
}
