﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class Impuestos
    {
        [MapFieldFromDataSet("totalImpuestosTrasladados", 0)]
        public decimal ImpuestosTrasladados { get; internal set; }
    }
}
