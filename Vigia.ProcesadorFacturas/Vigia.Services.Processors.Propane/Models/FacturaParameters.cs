﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class FacturaParameters
    {
        [MapFieldFromDataSet(0, "F2_ETIQ01", " ")]
        public string StrF2_ETIQ01 { get; set; }

        [MapFieldFromDataSet(0, "F2_NOPED", " ")]
        public string StrF2_NOPED { get; set; }

        [MapFieldFromDataSet(0, "F2_ETIQ02", " ")]
        public string StrF2_ETIQ02 { get; set; }

        [MapFieldFromDataSet(0, "F2_NOENT", " ")]
        public string StrF2_NOENT { get; set; }

        [MapFieldFromDataSet(0, "F2_ETIQ03", " ")]
        public string StrF2_ETIQ03 { get; set; }

        [MapFieldFromDataSet(0, "F2_NOREM", " ")]
        public string StrF2_NOREM { get; set; }

        [MapFieldFromDataSet(0, "A1_FMPAGO", " ")]
        public string StrA1_FMPAGO { get; set; }

        [MapFieldFromDataSet(0, "A1_MTPAGO", " ")]
        public string StrA1_MTPAGO { get; set; }

        [MapFieldFromDataSet(0, "A1_CTABCOP", " ")]
        public string StrA1_CTABCOP { get; set; }

        [MapFieldFromDataSet(0, "A1_BANCO", " ")]
        public string StrA1_BANCO { get; set; }

        [MapFieldFromDataSet(0, "A1_SUFRAMA", "")]
        public string StrA1_SUFRAMA { get; set; }

        [MapFieldFromDataSet(0, "E4_TPCPAG", " ")]
        public string StrE4_TPCPAG { get; set; }
    }
}
