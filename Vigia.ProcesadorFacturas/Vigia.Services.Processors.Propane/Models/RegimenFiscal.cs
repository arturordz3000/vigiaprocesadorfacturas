﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class RegimenFiscal
    {
        [MapFieldFromDataSet(field: "Regimen", defaultValue: "")]
        public string Regimen { get; internal set; }
    }
}
