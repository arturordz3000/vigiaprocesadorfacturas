﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vigia.Services.Common.Configuration;

namespace Vigia.ProcesadorFacturas.Service.Services
{
    internal interface IProcessorsConfigService
    {
        ProcessorConfiguration[] GetProcessorsConfiguration();
    }
}
