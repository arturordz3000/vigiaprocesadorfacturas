﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class Comprobante
    {
        [MapFieldFromDataSet(field: "version", defaultValue: "")]
        public string Version { get; set; }

        [MapFieldFromDataSet(field: "serie", defaultValue: "")]
        public string Serie { get; set; }

        [MapFieldFromDataSet(field: "folio", defaultValue: "")]
        public string Folio { get; set; }

        [MapFieldFromDataSet(field: "fecha", defaultValue: "")]
        public string Fecha { get; set; }

        [MapFieldFromDataSet(field: "sello", defaultValue: "")]
        public string Sello { get; set; }

        [MapFieldFromDataSet(field: "formaDePago", defaultValue: "")]
        public string FormaPago { get; set; }

        [MapFieldFromDataSet(field: "noCertificado", defaultValue: "")]
        public string NumCertificado { get; set; }

        [MapFieldFromDataSet(field: "certificado", defaultValue: "")]
        public string Certificado { get; set; }

        [MapFieldFromDataSet(field: "condicionesDePago", defaultValue: "")]
        public string CondicionesDePago { get; set; }

        [MapFieldFromDataSet("subTotal")]
        public decimal SubTotal { get; set; }

        [MapFieldFromDataSet("descuento")]
        public decimal Descuento { get; set; }

        [MapFieldFromDataSet("total")]
        public decimal Total { get; set; }

        [MapFieldFromDataSet(field: "metodoDePago", defaultValue: "")]
        public string MetodoDePago { get; set; }

        [MapFieldFromDataSet(field: "tipoDeComprobante", defaultValue: "")]
        public string TipoDeComprobante { get; set; }

        [MapFieldFromDataSet(field: "LugarExpedicion", defaultValue: "")]
        public string LugarExpedicion { get; set; }
    }
}
