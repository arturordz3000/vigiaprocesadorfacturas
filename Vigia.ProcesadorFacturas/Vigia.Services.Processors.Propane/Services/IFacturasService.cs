﻿using System.Data;
using Vigia.Common.Database;
using Vigia.Services.Processors.Propane.Models;

namespace Vigia.Services.Processors.Propane.Services
{
    internal interface IFacturasService
    {
        Factura[] GetFacturas();

        int InsertCfdi(Cfdi cfdi, IProcedureParametersAdapter<Cfdi> parametersAdapter);

        void InsertCfdiDetail(DataRow detailDataRow, IProcedureParametersAdapter<DataRow> parametersAdapter, IProcedureParametersAdapter<CfdiDetail> parametersAdapterForVersion3 = null);

        void SendCfdiByEmail(Cfdi cfdi);

        void InsertFacturaElectronica(Cfdi cfdi, IProcedureParametersAdapter<Cfdi> parametersAdapter);
    }
}
