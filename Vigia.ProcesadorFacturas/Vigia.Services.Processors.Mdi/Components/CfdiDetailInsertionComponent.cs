﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Mdi.Adapters;
using Vigia.Services.Processors.Mdi.Models;
using Vigia.Services.Processors.Mdi.Services;

namespace Vigia.Services.Processors.Mdi.Components
{
    internal class CfdiDetailInsertionComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IFacturasService facturasService;

        public CfdiDetailInsertionComponent(ILogFactory logFactory, IFacturasService facturasService)
        {
            logger = logFactory.Create(typeof(CfdiDetailInsertionComponent));
            this.facturasService = facturasService;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Cfdi[] cfdis = (Cfdi[])parameters[1];
            IList<Cfdi> successfulCfdis = new List<Cfdi>();

            foreach (Cfdi cfdi in cfdis)
            {
                logger.Info($"Insertando detalle de CFDi con Id: {cfdi.IdCfdi} para factura {cfdi.Factura.SerieFolio}...", sessionId);

                try
                {
                    if (DataSetContainsTable(cfdi.Factura.XmlFileDataSet, "Concepto", out DataTable detailDataTable))
                    {
                        foreach (DataRow detailDataRow in detailDataTable.Rows)
                        {
                            if (cfdi.IsVersion33)
                            {
                                facturasService.InsertCfdiDetail(detailDataRow,
                                    new InsertCfdiDetailsProcedureParametersAdapter(sessionId, cfdi.IdCfdi, InsertCfdiDetailsProcedureParametersAdapter.Case.Version3),
                                    new InsertCfdiDetailsForVersion3ProcedureParametersAdapter(sessionId, cfdi.IdCfdi));
                            }
                            else
                            {

                                facturasService.InsertCfdiDetail(detailDataRow,
                                    new InsertCfdiDetailsProcedureParametersAdapter(sessionId, cfdi.IdCfdi, InsertCfdiDetailsProcedureParametersAdapter.Case.AnyVersion));
                            }
                        }
                    }
                    else
                    {
                        if (TableNamesAreEqual(cfdi.Factura.XmlFileDataSet, 8, 20, out DataTable altDetailDataTable))
                        {
                            foreach (DataRow detailDataRow in altDetailDataTable.Rows)
                            {
                                facturasService.InsertCfdiDetail(detailDataRow,
                                    new InsertCfdiDetailsProcedureParametersAdapter(sessionId, cfdi.IdCfdi, InsertCfdiDetailsProcedureParametersAdapter.Case.AnyVersion));
                            }
                        }
                    }

                    successfulCfdis.Add(cfdi);
                    logger.Info($"Detalle de CFDi con Id: {cfdi.IdCfdi} insertado para factura {cfdi.Factura.SerieFolio} exitosamente", sessionId);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error insertando detalle de CFDi con Id: {cfdi.IdCfdi} para factura {cfdi.Factura.SerieFolio}. Mensaje: {ex.Message}", sessionId);
                }
            }

            return successfulCfdis.ToArray();
        }

        private bool DataSetContainsTable(DataSet dataSet, string tableName, out DataTable table)
        {
            table = null;

            if (dataSet.Tables.Contains(tableName))
            {
                table = dataSet.Tables[tableName];
                return true;
            }

            return false;
        }

        private bool TableNamesAreEqual(DataSet dataSet, int tableIndex1, int tableIndex2, out DataTable table)
        {
            table = null;

            if (dataSet.Tables[tableIndex1].TableName == dataSet.Tables[tableIndex2].TableName)
            {
                table = dataSet.Tables[tableIndex1];
                return true;
            }

            return false;
        }
    }
}
