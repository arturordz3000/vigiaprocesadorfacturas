﻿using System;
using System.Data;
using System.Data.SqlClient;
using Vigia.Common.Mapping;

namespace Vigia.Common.Database
{
    public class SqlServerDatabase : IDatabase
    {
        private readonly string connectionString;
        private readonly IDataSetMapper dataSetMapper;

        public SqlServerDatabase(string connectionString, IDataSetMapper dataSetMapper)
        {
            this.connectionString = connectionString;
            this.dataSetMapper = dataSetMapper;
        }

        public void ExecuteNonQuery(string nonQuery, object parametersCollection)
        {
            SqlParameter[] parameters = (SqlParameter[])parametersCollection;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(nonQuery, sqlConnection);
                command.Parameters.AddRange(parameters);

                sqlConnection.Open();

                command.ExecuteNonQuery();

                sqlConnection.Close();
            }
        }

        public void ExecuteProcedure(string procedureName, object parametersCollection)
        {
            SqlParameter[] parameters = (SqlParameter[])parametersCollection;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(procedureName, sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddRange(parameters);

                sqlConnection.Open();

                command.ExecuteNonQuery();

                sqlConnection.Close();
            }
        }

        public T ExecuteQuery<T>(string query)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);
                DataSet dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet);
                var result = dataSetMapper.MapTo<T>(dataSet);

                sqlConnection.Close();

                return result;
            }
        }

        public void ExecuteQuery<T>(string query, T dataSet, string tableName) where T : DataSet
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);

                if (dataSet == null)
                    dataSet = (T)Activator.CreateInstance(typeof(T));

                sqlDataAdapter.Fill(dataSet, tableName);

                sqlConnection.Close();
            }
        }

        public void ExecuteQuery<T>(string query, T dataSet) where T : DataSet
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);

                if (dataSet == null)
                    dataSet = (T)Activator.CreateInstance(typeof(T));

                sqlDataAdapter.Fill(dataSet);

                sqlConnection.Close();
            }
        }

        public T ExecuteQueryScalar<T>(string query)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, sqlConnection);

                sqlConnection.Open();

                var result = command.ExecuteScalar();

                sqlConnection.Close();

                return (T)Convert.ChangeType(result, typeof(T));
            }
        }
    }
}
