﻿using Vigia.Common.Pipelining;

namespace Vigia.Common.Factories
{
    public interface IPipelineFactory
    {
        IPipeline Create();
    }
}
