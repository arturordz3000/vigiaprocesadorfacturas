﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vigia.Common.Mapping
{
    public interface IEnvelope<T>
    {
        T Unwrap();
    }
}
