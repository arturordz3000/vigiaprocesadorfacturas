﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Vigia.Common.Utils
{
    public static class XmlUtils
    {
        public static DataSet LoadXmlAsDataSet(string xmlPath, bool removeNamespaces = true)
        {
            DataSet dataSet = new DataSet();

            if (removeNamespaces)
                LoadByRemovingNamespaces(xmlPath, dataSet);
            else
                dataSet.ReadXml(xmlPath);

            return dataSet;
        }

        private static void LoadByRemovingNamespaces(string xmlPath, DataSet dataSet)
        {
            string input = File.ReadAllText(xmlPath);
            string pattern = @"(</?)(\w+:)";

            string output = Regex.Replace(input, pattern, "$1");

            StringReader reader = new StringReader(output);

            dataSet.ReadXml(reader);
        }
    }
}
