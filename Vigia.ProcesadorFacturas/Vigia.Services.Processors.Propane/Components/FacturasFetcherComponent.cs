﻿using System;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Propane.Services;

namespace Vigia.Services.Processors.Propane.Components
{
    internal class FacturasFetcherComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IFacturasService facturasService;

        public FacturasFetcherComponent(ILogFactory logFactory, IFacturasService facturasService)
        {
            logger = logFactory.Create(typeof(FacturasFetcherComponent));
            this.facturasService = facturasService;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];

            logger.Info("Obteniendo facturas...", sessionId);

            var facturas = facturasService.GetFacturas();

            logger.Info($"Facturas obtenidas exitosamente. Numero de facturas: {facturas.Length}", sessionId);

            return facturas;
        }
    }
}
