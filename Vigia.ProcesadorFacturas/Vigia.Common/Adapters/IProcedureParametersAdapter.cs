﻿namespace Vigia.Common.Database
{
    public interface IProcedureParametersAdapter<T>
    {
        object[] Adapt(T toAdapt);
    }
}
