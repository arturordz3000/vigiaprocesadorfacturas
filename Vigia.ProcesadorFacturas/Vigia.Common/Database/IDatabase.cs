﻿using System.Data;

namespace Vigia.Common.Database
{
    public interface IDatabase
    {
        T ExecuteQuery<T>(string query);

        void ExecuteQuery<T>(string query, T dataSet, string tableName) where T : DataSet;

        void ExecuteQuery<T>(string query, T dataSet) where T : DataSet;

        T ExecuteQueryScalar<T>(string query);

        void ExecuteProcedure(string procedureName, object parametersCollection);

        void ExecuteNonQuery(string nonQuery, object parametersCollection);
    }
}
