﻿using Vigia.Common.Configuration;

namespace Vigia.Services.Processors.Propane.Services.Queries
{
    internal static class FacturasServiceQueries
    {
#if DEBUG
        private const string _GetFacturasQuery = "SELECT fac.E1_FILIAL,convert(char(2),rtrim(fac.E1_PREFIXO)) as SERIE, " +
              "  fac.E1_NUM as FOLIO, fac.E1_CLIENTE, fac.E1_LOJA, fac.E1_VALOR, fac.E1_TIPO, " +
              " SA1.A1_EMAIL as Email,  /* SA1.A1_VGFEMAI as Email */" +
              " /*qr.FECHA_GENERADO, */  '1' as qrCodeSW, ISNULL(SA1.A1_CLAVEMP + ' ' + SMP.MP_DESC, '') as PAGO_FISCAL, SE4.E4_TPCPAG " +
              " FROM SE1{0}0 fac " +
              " inner join SA1{0}0 SA1 on SA1.A1_COD=fac.E1_CLIENTE AND SA1.A1_LOJA=fac.E1_LOJA AND SA1.A1_FILIAL=fac.E1_FILIAL and fac.D_E_L_E_T_<>'*'  " +
              " /* AND SA1.A1_VGFEIMP<>'T' */" +
              " /* inner join facturas_qrCodeMDIgenerado qr on qr.E1_PREFIXO=fac.E1_PREFIXO and qr.E1_NUM=fac.E1_NUM  */ " +
              " LEFT JOIN facturas_enviadas face ON fac.E1_PREFIXO=face.E1_PREFIXO AND fac.E1_NUM=face.E1_NUM " +
              " LEFT JOIN facturas_enviadas face2 ON fac.E1_PREFIXO=face2.E1_PREFIXO AND fac.E1_NUM=face2.E1_NUM and face.FECHA_ENVIO>face2.FECHA_ENVIO " +
              " LEFT JOIN SMP{0}0 SMP on SA1.A1_CLAVEMP=SMP.MP_CLAVE and SMP.D_E_L_E_T_ <>'*' " +
              " LEFT JOIN SE4{0}0 SE4 on SE4.E4_FILIAL=SA1.A1_FILIAL and SA1.A1_COND=SE4.E4_CODIGO " +
              " WHERE /*dbo.fnClienteEmail(E1_FILIAL,E1_CLIENTE,E1_LOJA)<>'' AND */face.E1_NUM is null and face2.FECHA_ENVIO is null" +
              " and fac.E1_PREFIXO in(select FP_SERIE from SFP{0}0 where FP_USOSR='1' and FP_ESP1='NF' and D_E_L_E_T_ <>'*') " +
              " and SA1.A1_EMAIL like '%@%' " +
              " and fac.E1_EMISSAO <= (CONVERT(char(10), GetDate()-30,112)) " +
              " and fac.E1_PREFIXO  not in ('DR','AJ','AH','GA','SA','RM','EE','OA','KA','IA','WA','XH','RB','NB','VM','KA') " +
        " and fac.E1_PREFIXO ='FE' and fac.E1_NUM in ('000664')      ";
#else
        private const string _GetFacturasQuery = "SELECT fac.E1_FILIAL,convert(char(2),rtrim(fac.E1_PREFIXO)) as SERIE, " +
              "  fac.E1_NUM as FOLIO, fac.E1_CLIENTE, fac.E1_LOJA, fac.E1_VALOR, fac.E1_TIPO, " +
              " SA1.A1_EMAIL as Email,  /* SA1.A1_VGFEMAI as Email */" +
              " /*qr.FECHA_GENERADO, */  '1' as qrCodeSW, ISNULL(SA1.A1_CLAVEMP + ' ' + SMP.MP_DESC, '') as PAGO_FISCAL, SE4.E4_TPCPAG " +
              " FROM SE1{0}0 fac " +
              " inner join SA1{0}0 SA1 on SA1.A1_COD=fac.E1_CLIENTE AND SA1.A1_LOJA=fac.E1_LOJA AND SA1.A1_FILIAL=fac.E1_FILIAL and fac.D_E_L_E_T_<>'*'  " +
              " /* AND SA1.A1_VGFEIMP<>'T' */" +
              " /* inner join facturas_qrCodeMDIgenerado qr on qr.E1_PREFIXO=fac.E1_PREFIXO and qr.E1_NUM=fac.E1_NUM  */ " +
              " LEFT JOIN facturas_enviadas face ON fac.E1_PREFIXO=face.E1_PREFIXO AND fac.E1_NUM=face.E1_NUM " +
              " LEFT JOIN facturas_enviadas face2 ON fac.E1_PREFIXO=face2.E1_PREFIXO AND fac.E1_NUM=face2.E1_NUM and face.FECHA_ENVIO>face2.FECHA_ENVIO " +
              " LEFT JOIN SMP{0}0 SMP on SA1.A1_CLAVEMP=SMP.MP_CLAVE and SMP.D_E_L_E_T_ <>'*' " +
              " LEFT JOIN SE4{0}0 SE4 on SE4.E4_FILIAL=SA1.A1_FILIAL and SA1.A1_COND=SE4.E4_CODIGO " +
              " WHERE /*dbo.fnClienteEmail(E1_FILIAL,E1_CLIENTE,E1_LOJA)<>'' AND */face.E1_NUM is null and face2.FECHA_ENVIO is null" +
              " and fac.E1_PREFIXO in(select FP_SERIE from SFP{0}0 where FP_USOSR='1' and FP_ESP1='NF' and D_E_L_E_T_ <>'*') " +
              " and SA1.A1_EMAIL like '%@%' " +
              " and fac.E1_EMISSAO >= (CONVERT(char(10), GetDate(),112)) " +
              " and fac.E1_PREFIXO  not in ('DR','AJ','AH','GA','SA','RM','EE','OA','KA','IA','WA','XH','RB','NB','VM','KA') ";
#endif

        public static string GetFacturasQuery { get { return string.Format(_GetFacturasQuery, ApplicationConfiguration.GetInstance().CompanyId); } }

        public const string InsertCfdiDetailStep1NonQuery = "insert into tmpFacturaDetallePropaneCFDi values(@factura_id,@sesion,@cantidad,@unidad,@descripcion,@valorUnitario,@importe)";

        public const string InsertCfdiDetailStep2NonQuery = "insert into tmpFacturaDetalle2PropaneCFDi values(@factura_id,@sesion,@detalle_id,@claveprodserv,@claveunidad)";

        public const string CfdiDetailIdentCurrentQuery = "select IDENT_CURRENT( 'tmpFacturaDetallePropaneCFDi' );";
    }
}
