﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Vigia.Services.Common.Configuration;

namespace Vigia.ProcesadorFacturas.Service.Runners
{
    internal class ProcessorRunner : IProcessorRunner
    {
        private readonly string processorAppPath;
        private readonly string processorAppFolder;

        public ProcessorRunner(string processorAppPath, string processorAppFolder)
        {
            this.processorAppPath = processorAppPath;
            this.processorAppFolder = processorAppFolder;
        }

        public void RunAndWait(ProcessorConfiguration processorConfiguration)
        {
            string paramsFileName = $"{processorAppFolder}params_for_{processorConfiguration.CompanyId}.json";
            File.WriteAllText(paramsFileName, JsonConvert.SerializeObject(processorConfiguration));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = processorAppPath;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            startInfo.Arguments = "--singleMode " + paramsFileName;

            using (Process process = Process.Start(startInfo))
            {
                process.WaitForExit();
            }
        }
    }
}
