﻿using System.Collections.Generic;
using System.Threading;

namespace Vigia.Common.Extensions
{
    public static class ThreadExtensions
    {
        public static void JoinAll(this IEnumerable<Thread> threads)
        {
            foreach (Thread t in threads)
                t.Join();
        }
    }
}
