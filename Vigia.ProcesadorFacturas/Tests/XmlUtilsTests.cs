﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Vigia.Common.Utils;

namespace Vigia.Tests
{
    [TestClass]
    public class XmlUtilsTests
    {
        [TestMethod]
        public void LoadXmlAsDataSet_Should_Load_Xml_When_Removing_Namespaces()
        {
            var dataSet = XmlUtils.LoadXmlAsDataSet("TestData/factura_propane.xml");
            Assert.IsNotNull(dataSet);
            Assert.IsTrue(dataSet.Tables.Count > 0);

            dataSet = XmlUtils.LoadXmlAsDataSet("TestData/factura_mdi.xml");
            Assert.IsNotNull(dataSet);
            Assert.IsTrue(dataSet.Tables.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void LoadXmlAsDataSet_When_Keeping_Namespaces_But_Has_Repeated_Namespaces_Throws_Exception()
        {
            var dataSet = XmlUtils.LoadXmlAsDataSet("TestData/factura_propane.xml", false);
        }

        [TestMethod]
        public void LoadXmlAsDataSet_Should_Load_Xml_When_Keeping_Namespaces()
        {
            var dataSet = XmlUtils.LoadXmlAsDataSet("TestData/factura_mdi.xml", false);
            Assert.IsNotNull(dataSet);
            Assert.IsTrue(dataSet.Tables.Count > 0);
        }
    }
}
