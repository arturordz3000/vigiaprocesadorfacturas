﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Vigia.Common.Attributes;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Common.Mapping;
using Vigia.Services.Processors.Mdi.Models;

namespace Vigia.Services.Processors.Mdi.Components
{
    internal class CfdiBuilderComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IDataSetMapper dataSetMapper;

        public CfdiBuilderComponent(ILogFactory logFactory, IDataSetMapper dataSetMapper)
        {
            logger = logFactory.Create(typeof(CfdiBuilderComponent));
            this.dataSetMapper = dataSetMapper;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Factura[] facturas = (Factura[])parameters[1];
            IList<Cfdi> successfulCfdis = new List<Cfdi>();

            foreach (Factura factura in facturas)
            {
                try
                {
                    logger.Info($"Creando CFDi para factura {factura.SerieFolio}...", sessionId);

                    successfulCfdis.Add(BuildCfdiFromFactura(factura));

                    logger.Info($"CFDi para factura {factura.SerieFolio} creado exitosamente", sessionId);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error creando CFDi para factura {factura.SerieFolio}. Mensaje {ex.Message}", sessionId);
                }
            }

            return successfulCfdis.ToArray();
        }

        private Cfdi BuildCfdiFromFactura(Factura factura)
        {
            var cfdi = dataSetMapper.MapTo<Cfdi>(factura.XmlFileDataSet);

            if (cfdi.IsVersion33)
                HandleDistinctVersioning(cfdi, factura.XmlFileDataSet);

            cfdi.Factura = factura;

            return cfdi;
        }

        private void HandleDistinctVersioning(Cfdi cfdi, DataSet xmlFileDataSet)
        {
            cfdi.Comprobante.FormaPago = new MapFieldFromDataSetAttribute("Comprobante", "FormaPago").GetMappedValue(xmlFileDataSet).ToString();
            cfdi.Comprobante.MetodoDePago = new MapFieldFromDataSetAttribute("Comprobante", "MetodoPago").GetMappedValue(xmlFileDataSet).ToString();
            cfdi.ExpedidoEn.Municipio = cfdi.Comprobante.LugarExpedicion;
            cfdi.RegimenFiscal.Regimen = cfdi.Emisor.RegimenFiscal;
            cfdi.Impuestos.ImpuestosTrasladados = cfdi.Comprobante.Total - cfdi.Comprobante.SubTotal;
        }
    }
}
