﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vigia.Common.Database;
using Vigia.Common.Factories;
using Vigia.Common.Mapping;
using Vigia.ProcesadorFacturas.Service.Configuration;
using Vigia.ProcesadorFacturas.Service.Managers;
using Vigia.ProcesadorFacturas.Service.Runners;
using Vigia.ProcesadorFacturas.Service.Services;

namespace Vigia.ProcesadorFacturas.Service.Injection
{
    internal class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogFactory>().To<Log4NetLogFactory>();
            Bind<IProcessorsConfigService>().To<ProcessorsConfigService>();
            Bind<IDatabase>().To<SqlServerDatabase>().WithConstructorArgument("connectionString", AppSettings.DatabaseConnectionString);
            Bind<IProcessorsManager>().To<ProcessorsManager>();
            Bind<IProcessorRunner>().To<ProcessorRunner>()
                .WithConstructorArgument("processorAppPath", AppSettings.ProcessorAppPath)
                .WithConstructorArgument("processorAppFolder", AppSettings.ProcessorAppFolder);
            Bind<IDataSetMapper>().To<AttributeBasedDataSetMapper>();
        }
    }
}
