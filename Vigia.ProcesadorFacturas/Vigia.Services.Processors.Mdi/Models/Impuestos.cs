﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class Impuestos
    {
        [MapFieldFromDataSet("totalImpuestosTrasladados", 0)]
        public decimal ImpuestosTrasladados { get; internal set; }
    }
}
