﻿namespace Vigia.Common.Processing
{
    public interface IProcessor
    {
        void Run(bool isSingleMode = false);

        void Stop();
    }
}
