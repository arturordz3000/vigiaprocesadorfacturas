﻿using System.Data;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class CfdiDetail
    {
        public int CfdiDetailId { get; set; }

        public DataRow DetailDataRow { get; set; }
    }
}
