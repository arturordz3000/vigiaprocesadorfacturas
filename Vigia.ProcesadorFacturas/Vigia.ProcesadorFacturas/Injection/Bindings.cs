﻿using System.IO;
using Ninject.Modules;
using Vigia.Common.Configuration;
using Vigia.Common.Factories;
using Vigia.Common.Loaders;
using Vigia.Common.Logging;
using Vigia.Common.Processing;

namespace Vigia.ProcesadorFacturas.Injection
{
    internal class Bindings : NinjectModule
    {
        public override void Load()
        {
            BindProcessorsLoader();
            Bind<ILogFactory>().To<Log4NetLogFactory>();
            Bind<ILog>().To<Log4NetLog>();
        }

        private void BindProcessorsLoader()
        {
            Bind<ILoader<IProcessor[]>>().To<StandardProcessorsLoader>();
        }
    }
}
