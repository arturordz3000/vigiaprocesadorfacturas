﻿using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.ProcesadorFacturas.Service.Configuration;
using Vigia.ProcesadorFacturas.Service.Injection;
using Vigia.ProcesadorFacturas.Service.Managers;
using Vigia.ProcesadorFacturas.Service.Services;

namespace Vigia.ProcesadorFacturas.Service
{
    public partial class VigiaFacturasService : ServiceBase
    {
        private Timer timer;
        IKernel injectionKernel = new StandardKernel(new Bindings());
        private IProcessorsConfigService processorsConfigurationService;
        private IProcessorsManager processorsManager;
        private ILog log;

        public VigiaFacturasService()
        {
            InitializeComponent();
            log = injectionKernel.Get<ILogFactory>().Create(typeof(VigiaFacturasService));

            try
            {
                processorsConfigurationService = injectionKernel.Get<IProcessorsConfigService>();
                processorsManager = injectionKernel.Get<IProcessorsManager>();
            }
            catch (Exception ex)
            {
                log.Error($"Error al iniciar dependencias: {ex.Message}");
                throw ex;
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                processorsManager.Start(processorsConfigurationService.GetProcessorsConfiguration());

                timer = new Timer(AppSettings.TimerIntervalInSeconds * 1000);
                timer.Elapsed += UpdateConfiguration;
                timer.Start();
            }
            catch (Exception ex)
            {
                log.Error($"Error al iniciar servicio: {ex.Message}. Stacktrace:\n" + ex.StackTrace);
                throw ex;
            }
        }

        private void UpdateConfiguration(Object source, ElapsedEventArgs e)
        {
            try
            {
                log.Info("Actualizando configuracion de procesos desde base de datos...");

                var processorsConfiguration = processorsConfigurationService.GetProcessorsConfiguration();
                processorsManager.UpdateConfiguration(processorsConfiguration);

                log.Info("Configuracion de procesos actualizada correctamente!");
            }
            catch (Exception ex)
            {
                log.Error($"Error al actualizar configuracion de procesos desde base de datos: {ex.Message}");
            }
        }        

        protected override void OnStop()
        {
            try
            {
                log.Info("Deteniendo servicio...");

                timer.Stop();
                processorsManager.StopAndWait();

                log.Info("Servicio detenido correctamente!");
            }

            catch (Exception ex)
            {
                log.Error($"Error al detener servicio: {ex.Message}");
                throw ex;
            }
        }
    }
}
