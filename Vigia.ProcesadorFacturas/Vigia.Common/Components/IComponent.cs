﻿namespace Vigia.Common.Components
{
    public interface IComponent
    {
        object Execute(params object[] parameters);
    }
}
