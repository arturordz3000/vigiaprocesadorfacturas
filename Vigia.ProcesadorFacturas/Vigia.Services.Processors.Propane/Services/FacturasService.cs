﻿using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Vigia.Common.Configuration;
using Vigia.Common.Database;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Propane.Models;
using Vigia.Services.Processors.Propane.Services.Queries;

namespace Vigia.Services.Processors.Propane.Services
{
    internal class FacturasService : IFacturasService
    {
        private readonly ApplicationConfiguration applicationConfiguration;
        private readonly IDatabase database;
        private readonly ILog logger;

        public FacturasService(IDatabaseFactory databaseFactory, ILogFactory logFactory)
        {
            applicationConfiguration = ApplicationConfiguration.GetInstance();
            database = databaseFactory.Create(applicationConfiguration.TermogasDatabase);
            logger = logFactory.Create(typeof(FacturasService));
        }

        public Factura[] GetFacturas()
        {
            return database.ExecuteQuery<Factura[]>(FacturasServiceQueries.GetFacturasQuery);
        }

        public int InsertCfdi(Cfdi cfdi, IProcedureParametersAdapter<Cfdi> parametersAdapter)
        {
            var parameters = (SqlParameter[])parametersAdapter.Adapt(cfdi);

            database.ExecuteProcedure("pTmpFacturaAgregarPropaneCFDi", parameters);

            return (int)parameters.Where(p => p.ParameterName == "@factura_id").FirstOrDefault().Value;
        }

        public void InsertCfdiDetail(DataRow detailDataRow, IProcedureParametersAdapter<DataRow> parametersAdapter, IProcedureParametersAdapter<CfdiDetail> parametersAdapterForVersion3 = null)
        {
            database.ExecuteNonQuery(FacturasServiceQueries.InsertCfdiDetailStep1NonQuery, parametersAdapter.Adapt(detailDataRow));
            int cfdiDetailId = database.ExecuteQueryScalar<int>(FacturasServiceQueries.CfdiDetailIdentCurrentQuery);

            if (parametersAdapterForVersion3 != null)
            {
                CfdiDetail cfdiDetail = new CfdiDetail { CfdiDetailId = cfdiDetailId, DetailDataRow = detailDataRow };
                database.ExecuteNonQuery(FacturasServiceQueries.InsertCfdiDetailStep2NonQuery, parametersAdapterForVersion3.Adapt(cfdiDetail));
            }
        }

        public void InsertFacturaElectronica(Cfdi cfdi, IProcedureParametersAdapter<Cfdi> parametersAdapter)
        {
            database.ExecuteProcedure("pFacturaElectronicaSZJA10_Propane", parametersAdapter.Adapt(cfdi));
        }

        public void SendCfdiByEmail(Cfdi cfdi)
        {
            string smtpServer = applicationConfiguration.SmtpServer.Trim();
            int port = applicationConfiguration.Port;
            string user = applicationConfiguration.User.Trim();
            string password = applicationConfiguration.Password.Trim();
            string mailFrom = applicationConfiguration.MailFrom.Trim();

            logger.Info($"Preparando envio de correo con los siguientes datos: SmtpServer: {smtpServer}, Port: {port}, User: {user}, Password: {password}, MailFrom: {mailFrom}");

            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(mailFrom);

            var targetMailAddresses = cfdi.Factura.Email.Split(';');
            foreach (var targetMailAddress in targetMailAddresses)
                mailMessage.To.Add(new MailAddress(targetMailAddress));

            var copyMailAddresses = !string.IsNullOrEmpty(applicationConfiguration.SendMailCopyTo.Trim()) ? applicationConfiguration.SendMailCopyTo.Split(';') : new string[] { };
            foreach (var copyMailAddress in copyMailAddresses)
                mailMessage.Bcc.Add(new MailAddress(copyMailAddress));

            mailMessage.Subject = "Factura Electronica Propane Services SA de CV";
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = $"Le estamos enviado la factura {cfdi.Factura.SerieFolio}";

            string pdfRoute = $"{applicationConfiguration.PdfDirectoryPath}/F{cfdi.Factura.SerieFolio}.pdf";
            string pdfRoute2 = $"{applicationConfiguration.PdfDirectoryPath2}/FACTURA_{cfdi.Factura.SerieFolio}.pdf";
            string finalPdfRoute = cfdi.IsVersion33 && !File.Exists(pdfRoute) ? pdfRoute2 : pdfRoute;
            string xmlRoute = $"{applicationConfiguration.XmlFilesDirectoryPath}/Factura_{cfdi.Factura.Serie.Trim()}_{cfdi.Factura.Folio.Trim()}.xml";
            string remisionRoute = $"{applicationConfiguration.PdfDirectoryPath}/R{cfdi.Factura.SerieFolio}.pdf";
            string ticketRoute = $"{applicationConfiguration.PdfDirectoryPath}/T{cfdi.Factura.SerieFolio}.pdf";

            if (File.Exists(finalPdfRoute))
                mailMessage.Attachments.Add(new Attachment(finalPdfRoute));
            if (File.Exists(xmlRoute))
                mailMessage.Attachments.Add(new Attachment(xmlRoute));
            if (File.Exists(remisionRoute))
                mailMessage.Attachments.Add(new Attachment(remisionRoute));
            if (File.Exists(ticketRoute))
            mailMessage.Attachments.Add(new Attachment(ticketRoute));

            SmtpClient client = new SmtpClient(smtpServer, port);
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(user, password);

            client.Send(mailMessage);
        }
    }
}
