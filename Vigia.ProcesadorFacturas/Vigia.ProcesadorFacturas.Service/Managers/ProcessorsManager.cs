﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.ProcesadorFacturas.Service.Runners;
using Vigia.Services.Common.Configuration;

namespace Vigia.ProcesadorFacturas.Service.Managers
{
    internal class ProcessorsManager : IProcessorsManager
    {
        private bool isRunning = true;
        private ConcurrentDictionary<string, ProcessorConfiguration> processorsConfigurationByCompany;
        private IProcessorRunner processorRunner;
        private ILog log;

        public ProcessorsManager(IProcessorRunner processorRunner, ILogFactory logFactory)
        {
            this.processorRunner = processorRunner;
            log = logFactory.Create(typeof(ProcessorsManager));
        }

        public void Start(ProcessorConfiguration[] processorsConfiguration)
        {
            log.Info("Iniciando Process Manager...");

            // We use a concurrent dictionary since this is going to be used by multiple threads.
            processorsConfigurationByCompany = new ConcurrentDictionary<string, ProcessorConfiguration>(processorsConfiguration.ToDictionary(c => c.CompanyId));

            foreach (var companyId in processorsConfigurationByCompany.Keys)
            {
                log.Info($"Iniciando thread para empresa {companyId}...");

                Thread runnerThread = new Thread(new ParameterizedThreadStart(RunnerThread));
                runnerThread.Start(companyId);

                log.Info($"Thread para empresa {companyId} ha iniciado!");
            }
        }

        private void RunnerThread(object companyId)
        {
            ProcessorConfiguration configuration;
            while (isRunning && processorsConfigurationByCompany.TryGetValue((string)companyId, out configuration))
            {
                if (CanRunProcess(configuration))
                {
                    try
                    {
                        log.Info($"Ejecutando procesador para empresa {companyId}.");

                        configuration.LastExecuteDateTime = DateTime.Now;
                        processorRunner.RunAndWait(configuration);
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Ha ocurrido un error al ejecutar procesador para empresa {companyId}: {ex.Message}");
                    }
                }

                Thread.Sleep(1000);
            }

            processorsConfigurationByCompany.TryRemove((string)companyId, out configuration);

            log.Info($"Thread para empresa {companyId} se ha detenido.");
        }

        private bool CanRunProcess(ProcessorConfiguration configuration)
        {
            if (configuration.LastExecuteDateTime == null)
                return true;

            var diff = (DateTime.Now - configuration.LastExecuteDateTime.Value).TotalSeconds;

            return diff > configuration.ExecutionIntervalInSeconds;
        }

        public void StopAndWait()
        {
            log.Info($"Deteniendo Process Manager. Los procesos de cada empresa se detendran cuando terminen sus tareas en ejecucion...");

            isRunning = false;

            while (processorsConfigurationByCompany.Count > 0)
            {
                Thread.Sleep(1000);
            }

            log.Info($"Process Manager detenido correctamente!");
        }

        public void UpdateConfiguration(ProcessorConfiguration[] processorsConfiguration)
        {
            UpdateExistingConfigurations(processorsConfiguration);
            RemoveNonExistingConfigurations(processorsConfiguration);
        }

        private void UpdateExistingConfigurations(ProcessorConfiguration[] processorsConfiguration)
        {
            foreach (var configuration in processorsConfiguration)
            {
                if (processorsConfigurationByCompany.ContainsKey(configuration.CompanyId))
                {
                    configuration.LastExecuteDateTime = processorsConfigurationByCompany[configuration.CompanyId].LastExecuteDateTime;
                    processorsConfigurationByCompany[configuration.CompanyId] = configuration;
                }
            }
        }

        private void RemoveNonExistingConfigurations(ProcessorConfiguration[] processorsConfiguration)
        {
            var unexistingKeys = processorsConfigurationByCompany
                .Where(pair => !processorsConfiguration.Any(configuration => configuration.CompanyId == pair.Key))
                .Select(pair => pair.Key);

            foreach (var keyToDelete in unexistingKeys)
            {
                ProcessorConfiguration configuration;
                processorsConfigurationByCompany.TryRemove(keyToDelete, out configuration);
            }
        }
    }
}
