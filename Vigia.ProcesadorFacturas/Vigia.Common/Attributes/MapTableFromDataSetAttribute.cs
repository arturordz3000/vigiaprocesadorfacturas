﻿using System;
using System.Data;
using System.Reflection;
using Vigia.Common.Extensions;

namespace Vigia.Common.Attributes
{
    public class MapTableFromDataSetAttribute : MapTableFromDataSetBaseAttribute
    {
        public readonly string tableName;
        private readonly int tableIndex;
        private readonly bool isTableNameSet;


        public MapTableFromDataSetAttribute(string tableName)
        {
            this.tableName = tableName;
            isTableNameSet = true;
        }

        public MapTableFromDataSetAttribute(int tableIndex)
        {
            this.tableIndex = tableIndex;
            isTableNameSet = false;
        }

        public override void MapAllFields(object currentObject, DataSet dataSet, int rowIndex = 0)
        {
            MemberInfo[] members = currentObject.GetType().GetMembers();

            foreach (MemberInfo member in members)
            {
                MapFieldFromDataSetBaseAttribute fieldMappingAttribute = member.GetCustomAttribute<MapFieldFromDataSetBaseAttribute>();

                if (fieldMappingAttribute != null)
                {
                    if (isTableNameSet)
                        member.SetValue(currentObject, fieldMappingAttribute.GetMappedValue(tableName, dataSet, rowIndex));
                    else
                        member.SetValue(currentObject, fieldMappingAttribute.GetMappedValue(tableIndex, dataSet, rowIndex));
                }
            }
        }
    }
}
