﻿using System;
using System.Data;

namespace Vigia.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public abstract class MapFieldFromDataSetBaseAttribute : Attribute
    {
        public abstract object GetMappedValue(DataSet dataSet, int rowIndex = 0);
        public abstract object GetMappedValue(string tableName, DataSet dataSet, int rowIndex = 0);
        public abstract object GetMappedValue(int tableIndex, DataSet dataSet, int rowIndex = 0);
    }
}
