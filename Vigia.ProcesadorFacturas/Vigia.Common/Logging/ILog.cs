﻿using System;

namespace Vigia.Common.Logging
{
    public interface ILog
    {
        void Error(string message);
        void Error(string message, Guid sessionId);
        void Warning(string message);
        void Warning(string message, Guid sessionId);
        void Info(string message);
        void Info(string message, Guid sessionId);
        void Debug(string message);
        void Debug(string message, Guid sessionId);
    }
}
