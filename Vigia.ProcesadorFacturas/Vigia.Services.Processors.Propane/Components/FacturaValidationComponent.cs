﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Vigia.Common.Components;
using Vigia.Common.Configuration;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Common.Utils;
using Vigia.Services.Processors.Propane.Models;

namespace Vigia.Services.Processors.Propane.Components
{
    public class FacturaValidationComponent : IComponent
    {
        private readonly string qrCodesDirectoryPath;
        private readonly string xmlFilesDirectoryPath;
        private readonly ILog logger;

        public FacturaValidationComponent(ILogFactory logFactory)
        {
            qrCodesDirectoryPath = ApplicationConfiguration.GetInstance().QRCodesDirectoryPath;
            xmlFilesDirectoryPath = ApplicationConfiguration.GetInstance().XmlFilesDirectoryPath;
            logger = logFactory.Create(typeof(FacturaValidationComponent));
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Factura[] facturas = (Factura[])parameters[1];
            IList<Factura> successfulFacturas = new List<Factura>();

            foreach (var factura in facturas)
            {
                logger.Info($"Validando QRCode y Xml de factura {factura.SerieFolio}...", sessionId);

                try
                {
                    ValidateQRCodeAndSetFilePath(factura);
                    LoadAndValidateXmlFileDataSet(factura);

                    successfulFacturas.Add(factura);

                    logger.Info($"Factura {factura.SerieFolio} validada correctamente", sessionId);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            }

            return successfulFacturas.ToArray();
        }

        private void ValidateQRCodeAndSetFilePath(Factura factura)
        {
            string qrFilePath = $"{qrCodesDirectoryPath}/qrcode_{factura.SerieFolio}.jpg";

            if (!File.Exists(qrFilePath))
                throw new Exception($"No existe QRCode para factura {factura.SerieFolio}");

            factura.QRCodeFilePath = qrFilePath;
        }

        private void LoadAndValidateXmlFileDataSet(Factura factura)
        {
            factura.XmlFileDataSet = XmlUtils.LoadXmlAsDataSet($"{xmlFilesDirectoryPath}/Factura_{factura.Serie.Trim()}_{factura.Folio.Trim()}.xml");
            ValidateXmlFileField(factura, "TimbreFiscalDigital", "UUID");
        }

        private void ValidateXmlFileField(Factura factura, string table, string field)
        {
            bool isValid = !string.IsNullOrEmpty(factura.XmlFileDataSet.Tables["TimbreFiscalDigital"].Rows[0]["UUID"].ToString());

            if (!isValid)
                throw new Exception($"No se encontro campo {table}.{field} en factura {factura.SerieFolio}");
        }
    }
}
