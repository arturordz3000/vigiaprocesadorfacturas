﻿using System;
using Vigia.Common.Configuration;
using Vigia.Services.Processors.Propane.Models;

namespace Vigia.Services.Processors.Propane.Services.Queries
{
    internal class ReportsServiceQueries
    {
        public static string DescripcionMetodoPagoQuery(string codigoMetodoPago)
        {
            string companyId = ApplicationConfiguration.GetInstance().CompanyId;

            return $"Select ZSD_DESC From ZSD060 Where ZSD_CODIGO='{codigoMetodoPago}' and D_E_L_E_T_<>'*'";
        }

        public static string DatosFacturaQuery(Guid session)
        {
            return $"select * from [DATOSADV12].[dbo].[fnFEInformacionFacturaPropaneCFDi] ('{session}')";
        }

        public static string DatosDetalleFacturaQuery(Cfdi cfdi)
        {
            if (cfdi.IsVersion33)
            {
                return "Select fd.factura_id, fd.sesion, fd.detalle_id, fd.cantidad, fd.unidad, fd.descripcion, fd.valorunitario, fd.importe, fd2.claveprodserv, fd2.claveunidad " +
                "from tmpFacturaDetallePropaneCFDi fd inner join tmpFacturaDetalle2PropaneCFDi fd2 on fd.factura_id=fd2.factura_id and fd.sesion=fd2.sesion " +
                $"and fd.detalle_id=fd2.detalle_id where fd.factura_id={cfdi.IdCfdi}";
            }

            return "select factura_id, sesion, detalle_id, cantidad, unidad, descripcion, valorunitario, importe " +
                   $"from tmpFacturaDetallePropaneCFDi where factura_id={cfdi.IdCfdi}";
        }

        public static string FacturaParametersQuery(Cfdi cfdi)
        {
            string companyId = ApplicationConfiguration.GetInstance().CompanyId;

            return " Select SF2.F2_FILIAL, SF2.F2_CLIENTE, SF2.F2_LOJA, A1_FMPAGO, A1_MTPAGO, A1_CTABCOP, A1_BANCO, " +
                    "  SF2.F2_ETIQ01, SF2.F2_NOPED, SF2.F2_ETIQ02, SF2.F2_NOENT, SF2.F2_ETIQ03, SF2.F2_NOREM, SA1.A1_SUFRAMA " +
                    $"from SF2{companyId}0 SF2 inner join SA1{companyId}0 SA1 on SA1.A1_COD=SF2.F2_CLIENTE AND SA1.A1_LOJA=SF2.F2_LOJA AND SA1.A1_FILIAL=SF2.F2_FILIAL " +
                    $" where F2_SERIE='{cfdi.Factura.Serie}' and F2_DOC='{cfdi.Factura.Folio}'";
        }

        public static string RemisionesQuery(string serie, string folio)
        {
            string companyId = ApplicationConfiguration.GetInstance().CompanyId;

            return "select top 1000 SC6.C6_CLI as C6_CLI, SC6.C6_SERIE as C6_SERIE, SC6.C6_NOTA as C6_NOTA," +
                " CASE C6_NUMSZX WHEN ' ' Then C6_NUM ELSE C6_NUMSZX END as Remision, /*C6_NUMSZX,  C6_NUM,*/ " +
                " convert(char(10),cast( SC6.C6_EMISSAO AS datetime),103) as Fecha, SC6.C6_VALOR as Valor ," +
                " isnull(SZ9.Z9_DESCRI,'')   as Medidor, SC6.C6_QTDVEN as Litros, SA1.A1_NOME as Cliente " +
                $" from SA1{companyId}0 SA1" +
                $" inner join SC6{companyId}0 SC6 on SC6.C6_FILIAL=SA1.A1_FILIAL AND SC6.C6_CLI=SA1.A1_COD and SC6.C6_LOJA=SA1.A1_LOJA " +
                $" inner join SC5{companyId}0 SC5 on SC6.C6_FILIAL=SC5.C5_FILIAL AND SC6.C6_NUM=SC5.C5_NUM /*AND SC6.C6_CLIENTE=SC5.C5_CLIENTE*/ " +
                $" inner join SZ9{companyId}0 SZ9 on SC6.C6_FILIAL=SZ9.Z9_FILIAL AND SC5.C5_MEDIDOR=SZ9.Z9_CODIGO" +
                " where SC6.D_E_L_E_T_<>'*' and SC5.D_E_L_E_T_<>'*' and SA1.D_E_L_E_T_ <>'*' and SZ9.D_E_L_E_T_<>'*'" +
                $" and  SC6.C6_SERIE='{serie}' and SC6.C6_NOTA='{folio}' ";
        }

        public static string TicketsQuery(string serie, string folio)
        {
            string companyId = ApplicationConfiguration.GetInstance().CompanyId;

            return "select RTRIM(SA1.A1_END) + ' ' + ' #' + RTRIM(SA1.A1_NUMEXT) + ' ' + RTRIM(SA1.A1_BAIRRO) + ' ' + RTRIM(SA1.A1_MUN) + ' ' + ' ' + RTRIM(SA1.A1_ESTADO) + ' C.P. ' + RTRIM(SA1.A1_CEP) as A1_END, " +
              " SA1.A1_FILIAL + SA1.A1_COD as A1_COD , SA1.A1_LOJA, CASE C6_NUMSZX WHEN ' ' Then C6_NUM ELSE C6_NUMSZX END as Remision," +
              " convert(char(10),cast( SC5.C5_DATAI AS datetime),103) + ' ' + SC5.C5_HORAI as FechaImpresion,  " +
              " SC6.C6_QTDVEN as Litros, SC6.C6_PRCVEN, SC6.C6_VALOR as Valor,  " +
              " isnull(SZ9.Z9_DESCRI,'')   as Medidor, RTRIM(SA1.A1_NOME) as Cliente, RTRIM(SA1.A1_NREDUZ) AS A1_NREDUZ,  SE4.E4_COND   " +
             $" from SA1{companyId}0 SA1" +
             $" inner join SC6{companyId}0 SC6 on SC6.C6_FILIAL=SA1.A1_FILIAL AND SC6.C6_CLI=SA1.A1_COD and SC6.C6_LOJA=SA1.A1_LOJA " +
             $" inner join SC5{companyId}0 SC5 on SC6.C6_FILIAL=SC5.C5_FILIAL AND SC6.C6_NUM=SC5.C5_NUM /*AND SC6.C6_CLIENTE=SC5.C5_CLIENTE*/ " +
             $" inner join SZ9{companyId}0 SZ9 on SC6.C6_FILIAL=SZ9.Z9_FILIAL AND SC5.C5_MEDIDOR=SZ9.Z9_CODIGO" +
             $" left join SE4{companyId}0 SE4 on SA1.A1_COND=SE4.E4_CODIGO AND SA1.A1_FILIAL=SE4.E4_FILIAL AND SE4.D_E_L_E_T_ <>'*' " +
             " where SC6.D_E_L_E_T_<>'*' and SC5.D_E_L_E_T_<>'*' and SA1.D_E_L_E_T_ <>'*' and SZ9.D_E_L_E_T_<>'*'" +
             $" and  SC6.C6_SERIE='{serie}' and SC6.C6_NOTA='{folio}' ";
        }

        public static string FirmasQuery(string ticketNumber)
        {
            string companyId = ApplicationConfiguration.GetInstance().CompanyId;

            return "Select ISNULL(Firma_Digital,'') as Firma_Digital, ZX_NUMUDS, ZX_MEDIDOR, ZX_ODOMETR " +
                                        $" From SZX{companyId}0 ZX left join Firmas F on F.Id_Gaspar=ZX.ZX_NUM " +
                                         $" Where ZX_NUM='{ticketNumber}'";
        }
    }
}
