using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vigia.Common.Attributes;
using Vigia.Common.Mapping;

namespace Vigia.Tests
{
    [TestClass]
    public class AttributeBasedDataSetMapperTest
    {
        [TestMethod]
        public void MapTo_With_NormalClass_PropertiesAndMethods_Filled()
        {
            DataSet dataSet = CreateNormalClassDataSet();
            AttributeBasedDataSetMapper mapper = new AttributeBasedDataSetMapper();

            MockNormalClass instance = mapper.MapTo<MockNormalClass>(dataSet);

            Assert.IsNotNull(instance);
            Assert.AreEqual("String1", instance.StringField);
            Assert.AreEqual("String1", instance.IndexedTableStringProperty);
            Assert.AreEqual("String2", instance.StringProperty);
            Assert.AreEqual(123, instance.IntegerField);
            Assert.AreEqual(456, instance.IntegerProperty);
        }

        [TestMethod]
        public void MapTo_When_TableOrField_NotFound_Default_Value_Filled()
        {
            DataSet dataSet = CreateNormalClassDataSet();
            AttributeBasedDataSetMapper mapper = new AttributeBasedDataSetMapper();

            MockUnexistingTableOrFieldClass instance = mapper.MapTo<MockUnexistingTableOrFieldClass>(dataSet);

            Assert.IsNotNull(instance);
            Assert.AreEqual("", instance.StringField);
            Assert.AreEqual(123, instance.IntegerField);
            Assert.AreEqual(0, instance.IntegerProperty);
        }

        [TestMethod]
        public void MapTo_With_RecursiveClass_PropertiesAndMethods_Filled()
        {
            DataSet dataSet = CreateRecursiveClassDataSet();
            AttributeBasedDataSetMapper mapper = new AttributeBasedDataSetMapper();

            MockRecursiveTableClass instance = mapper.MapTo<MockRecursiveTableClass>(dataSet);

            Assert.IsNotNull(instance);
            Assert.IsNotNull(instance.childObject);
            Assert.AreEqual("String1", instance.StringField);
            Assert.AreEqual("String1", instance.childObject.StringField);
            Assert.AreEqual("String2", instance.childObject.StringProperty);
            Assert.AreEqual(123, instance.childObject.IntegerField);
            Assert.AreEqual(456, instance.childObject.IntegerProperty);
        }

        [TestMethod]
        public void MapTo_With_RecursiveClassArray_PropertiesAndMethods_Filled()
        {
            DataSet dataSet = CreateNormalClassArrayDataSet(5);
            AttributeBasedDataSetMapper mapper = new AttributeBasedDataSetMapper();

            MockNormalClass[] instance = mapper.MapTo<MockNormalClass[]>(dataSet);

            Assert.IsNotNull(instance);
            Assert.AreEqual(5, instance.Length);
            Assert.AreEqual("String1", instance[0].StringField);
            Assert.AreEqual("String2", instance[1].StringField);
            Assert.AreEqual("String3", instance[2].StringField);
            Assert.AreEqual("String4", instance[3].StringField);
            Assert.AreEqual("String5", instance[4].StringField);
        }

        private DataSet CreateNormalClassDataSet()
        {
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(new DataTable("Table1"));
            dataSet.Tables["Table1"].Columns.Add(new DataColumn("Field1", typeof(string)));
            dataSet.Tables["Table1"].Columns.Add(new DataColumn("Field2", typeof(string)));
            dataSet.Tables["Table1"].Columns.Add(new DataColumn("Field3", typeof(int)));
            dataSet.Tables["Table1"].Columns.Add(new DataColumn("Field4", typeof(int)));

            DataRow row = dataSet.Tables["Table1"].NewRow();
            row["Field1"] = "String1";
            row["Field2"] = "String2";
            row["Field3"] = 123;
            row["Field4"] = 456;

            dataSet.Tables["Table1"].Rows.Add(row);

            return dataSet;
        }

        private DataSet CreateRecursiveClassDataSet()
        {
            DataSet dataSet = CreateNormalClassDataSet();

            dataSet.Tables.Add(new DataTable("Table2"));
            dataSet.Tables["Table2"].Columns.Add(new DataColumn("Field1", typeof(string)));

            DataRow row = dataSet.Tables["Table2"].NewRow();
            row["Field1"] = "String1";

            dataSet.Tables["Table2"].Rows.Add(row);

            return dataSet;
        }

        private DataSet CreateNormalClassArrayDataSet(int count)
        {
            DataSet dataSet = CreateNormalClassDataSet();

            for (int i = 1; i < count; i++)
            {
                DataRow row = dataSet.Tables["Table1"].NewRow();
                row["Field1"] = "String" + (i + 1);

                dataSet.Tables["Table1"].Rows.Add(row);
            }

            return dataSet;
        }
    }

    public class MockNormalClass
    {
        [MapFieldFromDataSet("Table1", "Field1")]
        public string StringField;

        [MapFieldFromDataSet(0, "Field1")]
        public string IndexedTableStringProperty { get; set; }

        [MapFieldFromDataSet("Table1", "Field2")]
        public string StringProperty { get; set; }

        [MapFieldFromDataSet("Table1", "Field3")]
        public int IntegerField { get; set; }

        [MapFieldFromDataSet("Table1", "Field4")]
        public int IntegerProperty { get; set; }
    }

    public class MockRecursiveTableClass
    {
        [MapTableFromDataSet("Table1")]
        public MockTableClass childObject;

        [MapFieldFromDataSet("Table2", "Field1")]
        public string StringField;
    }

    public class MockTableClass
    {
        [MapFieldFromDataSet("Field1")]
        public string StringField;

        [MapFieldFromDataSet("Field1")]
        public string IndexedTableStringProperty { get; set; }

        [MapFieldFromDataSet("Field2")]
        public string StringProperty { get; set; }

        [MapFieldFromDataSet("Field3")]
        public int IntegerField { get; set; }

        [MapFieldFromDataSet("Field4")]
        public int IntegerProperty { get; set; }
    }

    public class MockUnexistingTableOrFieldClass
    {
        [MapFieldFromDataSet("UnexistingTable1", "UnexistingField1", "")]
        public string StringField;

        [MapFieldFromDataSet("UnexistingTable1", "UnexistingField2", 123)]
        public int IntegerField;

        [MapFieldFromDataSet("UnexistingTable1", "UnexistingField3")]
        public int IntegerProperty;
    }
}
