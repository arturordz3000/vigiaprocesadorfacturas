﻿using System.Data;

namespace Vigia.Common.Mapping
{
    public interface IDataSetMapper
    {
        T MapTo<T>(DataSet dataSet);
    }
}
