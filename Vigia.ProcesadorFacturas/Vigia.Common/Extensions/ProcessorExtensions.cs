﻿using System.Collections.Generic;
using System.Threading;
using Vigia.Common.Processing;

namespace Vigia.Common.Extensions
{
    public static class ProcessorExtensions
    {
        public static void RunAndWait(this IEnumerable<IProcessor> processors, bool isSingleMode = false)
        {
            var threads = Run(processors, isSingleMode);
            threads.JoinAll();
        }

        public static IList<Thread> Run(this IEnumerable<IProcessor> processors, bool isSingleMode = false)
        {
            IList<Thread> threads = new List<Thread>();

            foreach (var p in processors)
                threads.Add(RunProcessorInThread(p, isSingleMode));

            return threads;
        }

        private static Thread RunProcessorInThread(IProcessor processor, bool isSingleMode)
        {
            Thread thread = new Thread(() => processor.Run(isSingleMode));
            thread.Start();

            return thread;
        }
    }
}
