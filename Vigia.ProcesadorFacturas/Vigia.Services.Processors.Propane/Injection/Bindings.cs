﻿using Ninject.Modules;
using Vigia.Common.Factories;
using Vigia.Common.Mapping;
using Vigia.Services.Processors.Propane.Factories;
using Vigia.Services.Processors.Propane.Services;

namespace Vigia.Services.Processors.Propane.Injection
{
    internal class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IPipelineFactory>().To<StandardPipelineFactory>();
            Bind<ILogFactory>().To<Log4NetLogFactory>();
            Bind<IFacturasService>().To<FacturasService>();
            Bind<IReportsService>().To<CrystalReportsService>();
            Bind<IDatabaseFactory>().To<SqlServerDatabaseFactory>();
            Bind<IDataSetMapper>().To<AttributeBasedDataSetMapper>();
        }
    }
}
