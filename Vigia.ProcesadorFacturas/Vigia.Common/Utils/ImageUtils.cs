﻿using System.Drawing;

namespace Vigia.Common.Utils
{
    public class ImageUtils
    {
        public static void ResizeImageWhenOutOfBounds(string imagePath, int width, int height)
        {
            Bitmap bitmap = null;

            using (var image = Image.FromFile(imagePath))
            {
                if (image.Width <= width || image.Height <= height)
                    return;

                bitmap = new Bitmap(image, width, height);
            }

            bitmap.Save(imagePath);
        }
    }
}
