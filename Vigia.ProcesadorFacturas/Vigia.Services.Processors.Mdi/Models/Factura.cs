﻿using System.Data;
using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class Factura
    {
        [MapFieldFromDataSet(0, "SERIE")]
        public string Serie { get; set; }

        [MapFieldFromDataSet(0, "FOLIO")]
        public string Folio { get; set; }

        [MapFieldFromDataSet(0, "EMAIL")]
        public string Email { get; set; }

        [MapFieldFromDataSet(0, "qrCodeSW")]
        public string QrCodeSW { get; set; }

        [MapFieldFromDataSet(0, "PAGO_FISCAL")]
        public string PagoFiscal { get; set; }

        [MapFieldFromDataSet(0, "E4_TPCPAG")]
        public string E4_TPCPAG { get; set; }

        public string SerieFolio { get { return Serie + Folio; } }

        public DataSet XmlFileDataSet { get; set; }

        public string QRCodeFilePath { get; set; }

        [MapFieldFromDataSet(0, "E1_FILIAL")]
        public string E1_Filial { get; internal set; }

        [MapFieldFromDataSet(0, "E1_CLIENTE")]
        public string E1_Cliente { get; internal set; }

        [MapFieldFromDataSet(0, "E1_LOJA")]
        public string E1_Loja { get; internal set; }

        [MapFieldFromDataSet(0, "E1_VALOR")]
        public decimal E1_Valor { get; internal set; }

        [MapFieldFromDataSet(0, "E1_TIPO")]
        public string E1_Tipo { get; internal set; }
    }
}
