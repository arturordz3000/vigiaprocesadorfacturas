﻿namespace Vigia.Common.Pipelining
{
    public interface IPipeline
    {
        void Build();

        void Execute();
    }
}
