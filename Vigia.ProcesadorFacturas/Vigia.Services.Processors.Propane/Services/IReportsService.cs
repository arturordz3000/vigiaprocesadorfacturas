﻿using System;
using Vigia.Services.Processors.Propane.Models;

namespace Vigia.Services.Processors.Propane.Services
{
    internal interface IReportsService
    {
        void GenerateReports(Guid sessionId, Cfdi cfdi);
    }
}
