﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Microsoft.Reporting.WebForms;
using Vigia.Common.Configuration;
using Vigia.Common.Database;
using Vigia.Common.Extensions;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Common.Utils;
using Vigia.Services.Processors.Propane.Data;
using Vigia.Services.Processors.Propane.Models;
using Vigia.Services.Processors.Propane.Services.Queries;

namespace Vigia.Services.Processors.Propane.Services
{
    internal class CrystalReportsService : IReportsService
    {
        private readonly ApplicationConfiguration applicationConfiguration;
        private readonly IDatabase database;
        private readonly ILog logger;

        public CrystalReportsService(IDatabaseFactory databaseFactory, ILogFactory logFactory)
        {
            applicationConfiguration = ApplicationConfiguration.GetInstance();
            database = databaseFactory.Create(applicationConfiguration.TermogasDatabase);
            logger = logFactory.Create(typeof(CrystalReportsService));
        }

        public void GenerateReports(Guid sessionId, Cfdi cfdi)
        {
            PreparePdfData(sessionId, cfdi, out FacturasDataSet facturasDataSet, out FacturaParameters facturaParameters);
            CreateFacturaPdfReport(cfdi, facturasDataSet, facturaParameters);

            if (ApplicationConfiguration.GetInstance().SendRemisiones)
                CreateRemisionPdf(cfdi, facturasDataSet, facturaParameters);
            
            if (ApplicationConfiguration.GetInstance().SendTickets)
                CreateTicketPdf(cfdi, facturasDataSet, facturaParameters, 4);
        }

        private void PreparePdfData(Guid sessionId, Cfdi cfdi, out FacturasDataSet facturasDataSet, out FacturaParameters facturaParameters)
        {
            facturasDataSet = new FacturasDataSet();
            database.ExecuteQuery(ReportsServiceQueries.DatosFacturaQuery(sessionId), facturasDataSet, "dtDatosFactura");
            database.ExecuteQuery(ReportsServiceQueries.DatosDetalleFacturaQuery(cfdi), facturasDataSet, "dtDatosDetalleFactura");

            facturaParameters = database.ExecuteQuery<FacturaParameters>(ReportsServiceQueries.FacturaParametersQuery(cfdi));
            facturaParameters.StrE4_TPCPAG = cfdi.Factura.E4_TPCPAG;

            if (facturaParameters.StrA1_MTPAGO.Trim().Length == 0)
            {
                facturaParameters.StrA1_MTPAGO = "PAGO EN UNA SOLA EXHIBICION";
            }

            if (cfdi.Factura.PagoFiscal.Trim().Length == 0)
            {
                if (cfdi.Factura.E4_TPCPAG.Trim() == "1")
                    facturaParameters.StrA1_FMPAGO = "01 Efectivo.";
                else
                    facturaParameters.StrA1_FMPAGO = "98 (N/A)";
            }
            else
                facturaParameters.StrA1_FMPAGO = cfdi.Factura.PagoFiscal.Trim();

            if (cfdi.Factura.E4_TPCPAG.Trim() != "1")
            {
                if (cfdi.IsVersion33)
                {
                    string descripcionMetodoPago = database.ExecuteQueryScalar<string>(ReportsServiceQueries.DescripcionMetodoPagoQuery(cfdi.Comprobante.FormaPago));
                    facturaParameters.StrA1_FMPAGO = $"{cfdi.Comprobante.FormaPago} {descripcionMetodoPago}";
                    facturaParameters.StrA1_MTPAGO = cfdi.Comprobante.MetodoDePago;
                }
            }
        }

        private void CreateFacturaPdfReport(Cfdi cfdi, FacturasDataSet facturasDataSet, FacturaParameters facturaParameters)
        {
            using (ReportDocument report = new ReportDocument())
            {
                string reportPath = GetReportPath(cfdi);
                report.Load(reportPath);

                string qrImageFullPath = Path.GetFullPath(cfdi.Factura.QRCodeFilePath);

                try
                {
                    ImageUtils.ResizeImageWhenOutOfBounds(qrImageFullPath, 182, 182);
                }
                catch (Exception ex)
                {
                    logger.Warning($"Error al modificar tamaño de imagen {qrImageFullPath}. Se usará imagen original. Error: {ex.Message}");
                }

                report.SetDataSource(facturasDataSet);

                string current = "imageQRCode";
                try
                {
                    report.SetParameterValue("imageQRCode", qrImageFullPath);
                    current = "A1_FMPAGO";
                    report.SetParameterValue("A1_FMPAGO", facturaParameters.StrA1_FMPAGO);
                    current = "A1_MTPAGO";
                    report.SetParameterValue("A1_MTPAGO", facturaParameters.StrA1_MTPAGO);
                    current = "A1_CTABCOP";
                    report.SetParameterValue("A1_CTABCOP", facturaParameters.StrA1_CTABCOP);
                    current = "A1_BANCO";
                    report.SetParameterValue("A1_BANCO", facturaParameters.StrA1_BANCO);

                    if (cfdi.IsVersion33)
                    {
                        current = "TIPO_DE_COMPROBANTE";
                        report.SetParameterValue("TIPO_DE_COMPROBANTE", $"TIPO DE COMPROBANTE: {cfdi.Comprobante.TipoDeComprobante}");
                        current = "USO_CFDI";
                        report.SetParameterValue("USO_CFDI", $"USO DEL CFDI: {cfdi.Receptor.UsoCfdi}");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Fallo con: " + current + " path reporte: " + reportPath);
                    throw ex;
                }

                string facturaPdfFileName = BuildPdfFilePath("F", cfdi.Factura.SerieFolio);
                report.ExportToDisk(ExportFormatType.PortableDocFormat, facturaPdfFileName);
                report.Close();

                logger.Info($"Factura generada: {facturaPdfFileName}");
            }
        }

        private string GetReportPath(Cfdi cfdi)
        {
            string path = null;

            
            if (cfdi.IsVersion33)
                path = applicationConfiguration.RptFacturaCreditoV2;
            else if (cfdi.Factura.E4_TPCPAG.Trim() == "1")
                path = applicationConfiguration.RptFacturaContado;
            else
                path = applicationConfiguration.RptFacturaCredito;

            return Path.GetFullPath(path);
        }

        private void CreateRemisionPdf(Cfdi cfdi, FacturasDataSet facturasDataSet, FacturaParameters facturaParameters)
        {
            database.ExecuteQuery(ReportsServiceQueries.RemisionesQuery(cfdi.Factura.Serie, cfdi.Factura.Folio), facturasDataSet, "dtRemision");

            var remisionReportDataSource = new ReportDataSource();
            remisionReportDataSource.Name = "Remision";
            remisionReportDataSource.Value = facturasDataSet.dtRemision;

            var remisionReport = new LocalReport();
            remisionReport.ReportPath = applicationConfiguration.RemisionDirectoryPath;
            remisionReport.DataSources.Add(remisionReportDataSource);

            var remisionPdfFileName = BuildPdfFilePath("R", cfdi.Factura.SerieFolio);

            RenderLocalReportToFile(remisionReport, remisionPdfFileName);

            logger.Info($"Remision generada: {remisionPdfFileName}");
        }

        private void CreateTicketPdf(Cfdi cfdi, FacturasDataSet facturasDataSet, FacturaParameters facturaParameters, int columnRepeatCount)
        {
            DataTable ticketsDataTable = BuildTicketsDataTable(new string[] {
                "Domicilio",
                "Equipo",
                "ClaveCliente",
                "Ticket",
                "UnidadRecepcion",
                "IDGaspar",
                "NoServicio",
                "FechaServicio",
                "FechaImpresion",
                "Volumen",
                "PrecioUnitario",
                "ImporteTotal",
                "ImporteLetra",
                "Firma",
                "Cliente",
                "Descripcion",
                "FechaPago",
                "Odometro"
            }, columnRepeatCount);

            DataSet tickets = new DataSet();
            database.ExecuteQuery(ReportsServiceQueries.TicketsQuery(cfdi.Factura.Serie, cfdi.Factura.Folio), tickets, "Tickets");

            DataRow ticketDataRow = null;
            int rowCount = 0;

            foreach (DataRow currentTicketDataRow in tickets.Tables[0].Rows)
            {
                int columnIndex = rowCount % columnRepeatCount + 1;

                if (columnIndex == 1)
                    ticketDataRow = ticketsDataTable.NewRow();

                var paymentDate = DateTime.ParseExact(currentTicketDataRow[4].ToString(), "dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                ticketDataRow[$"FechaPago{columnIndex}"] = paymentDate.AddDays(double.Parse(currentTicketDataRow[11].ToString())).ToShortDateString();
                ticketDataRow[$"Domicilio{columnIndex}"] = currentTicketDataRow[0].ToString();
                ticketDataRow[$"Equipo{columnIndex}"] = currentTicketDataRow[8].ToString();
                ticketDataRow[$"ClaveCliente{columnIndex}"] = $"{currentTicketDataRow[1].ToString()}-{currentTicketDataRow[2].ToString()}";
                ticketDataRow[$"Ticket{columnIndex}"] = currentTicketDataRow[3].ToString();
                ticketDataRow[$"UnidadRecepcion{columnIndex}"] = currentTicketDataRow[0].ToString();
                ticketDataRow[$"IDGaspar{columnIndex}"] = "";
                ticketDataRow[$"NoServicio{columnIndex}"] = "NoServicio";
                ticketDataRow[$"FechaServicio{columnIndex}"] = currentTicketDataRow[4].ToString();
                ticketDataRow[$"FechaImpresion{columnIndex}"] = currentTicketDataRow[4].ToString();
                ticketDataRow[$"Volumen{columnIndex}"] = currentTicketDataRow[5].ToString();
                ticketDataRow[$"PrecioUnitario{columnIndex}"] = currentTicketDataRow[6].ToString();
                ticketDataRow[$"ImporteTotal{columnIndex}"] = currentTicketDataRow[7].ToString();
                ticketDataRow[$"ImporteLetra{columnIndex}"] = double.Parse(currentTicketDataRow[7].ToString()).GetCurrencyDescription();
                ticketDataRow[$"Firma{columnIndex}"] = "";
                ticketDataRow[$"Cliente{columnIndex}"] = currentTicketDataRow[9].ToString();
                ticketDataRow[$"Descripcion{columnIndex}"] = currentTicketDataRow[10].ToString();
                ticketDataRow[$"Odometro{columnIndex}"] = "";

                DataSet firmas = new DataSet();
                database.ExecuteQuery(ReportsServiceQueries.FirmasQuery(currentTicketDataRow[3].ToString()), firmas, "Firmas");

                foreach (DataRow firmaDataRow in firmas.Tables[0].Rows)
                {
                    ticketDataRow[$"IDGaspar{columnIndex}"] = firmaDataRow[1].ToString();
                    ticketDataRow[$"Firma{columnIndex}"] = firmaDataRow[0].ToString().Replace("\n", "");
                    ticketDataRow[$"Odometro{columnIndex}"] = firmaDataRow[3].ToString();
                }

                rowCount++;

                if (columnIndex >= columnRepeatCount || rowCount >= tickets.Tables[0].Rows.Count)
                    ticketsDataTable.Rows.Add(ticketDataRow);
            }

            ReportDataSource ticketsReportDataSource = new ReportDataSource();
            ticketsReportDataSource.Name = "Tickets";
            ticketsReportDataSource.Value = ticketsDataTable;

            LocalReport ticketsReport = new LocalReport();
            ticketsReport.ReportPath = applicationConfiguration.TicketsDirectoryPath;
            ticketsReport.DataSources.Add(ticketsReportDataSource);
            ticketsReport.EnableExternalImages = true;

            string ticketFileName = BuildPdfFilePath("T", cfdi.Factura.SerieFolio);
            RenderLocalReportToFile(ticketsReport, ticketFileName);

            logger.Info($"Ticket generado: {ticketFileName}");
        }

        private DataTable BuildTicketsDataTable(string[] columnNames, int columnRepeatCount)
        {
            DataTable ticketsDataTable = new DataTable();

            foreach (string column in columnNames)
            {
                for (int i = 1; i <= columnRepeatCount; i++)
                {
                    ticketsDataTable.Columns.Add($"{column}{i}");
                }
            }

            return ticketsDataTable;
        }

        private string BuildPdfFilePath(string prefix, string fileName)
        {
            return $"{applicationConfiguration.PdfDirectoryPath}/{prefix}{fileName}.pdf";
        }

        private void RenderLocalReportToFile(LocalReport localReport, string reportFileName)
        {
            var fileBytes = localReport.Render("PDF");

            using (var fileStream = File.Create(reportFileName))
            {
                fileStream.Write(fileBytes, 0, fileBytes.Length);
            }
        }
    }
}
