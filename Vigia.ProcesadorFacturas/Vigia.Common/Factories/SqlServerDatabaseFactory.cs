﻿using Vigia.Common.Database;
using Vigia.Common.Mapping;

namespace Vigia.Common.Factories
{
    public class SqlServerDatabaseFactory : IDatabaseFactory
    {
        private readonly IDataSetMapper mapper;

        public SqlServerDatabaseFactory(IDataSetMapper mapper)
        {
            this.mapper = mapper;
        }

        public IDatabase Create(string connectionString)
        {
            return new SqlServerDatabase(connectionString, mapper);
        }
    }
}
