﻿using System;
using Vigia.Services.Processors.Mdi.Models;

namespace Vigia.Services.Processors.Mdi.Services
{
    internal interface IReportsService
    {
        void GenerateReports(Guid sessionId, Cfdi cfdi);
    }
}
