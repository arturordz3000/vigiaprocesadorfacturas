﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Propane.Adapters;
using Vigia.Services.Processors.Propane.Models;
using Vigia.Services.Processors.Propane.Services;

namespace Vigia.Services.Processors.Propane.Components
{
    internal class CfdiInsertionComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IFacturasService facturasService;

        public CfdiInsertionComponent(ILogFactory logFactory, IFacturasService facturasService)
        {
            logger = logFactory.Create(typeof(CfdiInsertionComponent));
            this.facturasService = facturasService;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Cfdi[] cfdis = (Cfdi[])parameters[1];
            IList<Cfdi> successfulCfdis = new List<Cfdi>();

            foreach (Cfdi cfdi in cfdis)
            {
                try
                {
                    logger.Info($"Insertando CFDi para factura {cfdi.Factura.SerieFolio}...", sessionId);

                    cfdi.IdCfdi = facturasService.InsertCfdi(cfdi, new InsertCfdiProcedureParamteresAdapter(sessionId));
                    successfulCfdis.Add(cfdi);

                    logger.Info($"CFDi con Id: {cfdi.IdCfdi} insertado para factura {cfdi.Factura.SerieFolio} exitosamente", sessionId);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error insertando CFDi con Id: {cfdi.IdCfdi} para factura {cfdi.Factura.SerieFolio}. Mensaje: {ex.Message}", sessionId);
                }
            }

            return successfulCfdis.ToArray();
        }
    }
}
