﻿namespace Vigia.Common.Loaders
{
    public interface ILoader<T>
    {
        T Load();
    }
}
