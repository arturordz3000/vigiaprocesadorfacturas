﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class TimbreFiscalDigital
    {
        [MapFieldFromDataSet(field: "UUID", defaultValue: "")]
        public string Uuid { get; set; }

        [MapFieldFromDataSet(field: "FechaTimbrado", defaultValue: "")]
        public string FechaTimbrado { get; set; }

        [MapFieldFromDataSet(field: "selloCFD", defaultValue: "")]
        public string SelloCfd { get; set; }

        [MapFieldFromDataSet(field: "noCertificadoSAT", defaultValue: "")]
        public string NoCertificadoSat { get; set; }

        [MapFieldFromDataSet(field: "selloSAT", defaultValue: "")]
        public string SelloSat { get; set; }
    }
}
