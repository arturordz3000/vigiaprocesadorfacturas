﻿using System;
using System.Reflection;

namespace Vigia.Common.Extensions
{
    public static class MemberInfoExtensions
    {
        public static void SetValue(this MemberInfo member, object target, object value)
        {
            try
            {
                if (value == null || value.GetType() == typeof(DBNull))
                    return;

                value = ParseIfNumeric(value, member.GetVariableType());
                value = ParseIfBoolean(value, member.GetVariableType());

                switch (member.MemberType)
                {
                    case MemberTypes.Field:
                        ((FieldInfo)member).SetValue(target, value);
                        break;
                    case MemberTypes.Property:
                        ((PropertyInfo)member).SetValue(target, value);
                        break;
                    default:
                        throw new ArgumentException("MemberInfo must be if type FieldInfo or PropertyInfo", "member");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object GetValue(this MemberInfo member, object target)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)member).GetValue(target);
                case MemberTypes.Property:
                    return ((PropertyInfo)member).GetValue(target);
                default:
                    throw new ArgumentException("MemberInfo must be if type FieldInfo or PropertyInfo", "member");
            }
        }

        private static object ParseIfNumeric(object value, Type targetType)
        {
            if (targetType == typeof(Decimal))
                return Decimal.Parse(value.ToString());

            if (targetType == typeof(float))
                return float.Parse(value.ToString());

            if (targetType == typeof(int))
                return int.Parse(value.ToString());

            return value;
        }

        private static object ParseIfBoolean(object value, Type targetType)
        {
            if (targetType == typeof(bool))
            {
                string val = value.ToString();
                if (val == "1")
                    return true;
                else if (val == "0")
                    return false;
            }

            return value;
        }

        public static bool IsVariable(this MemberInfo member)
        {
            return member.MemberType == MemberTypes.Field || member.MemberType == MemberTypes.Property;
        }

        public static Type GetVariableType(this MemberInfo member)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)member).FieldType;
                case MemberTypes.Property:
                    return ((PropertyInfo)member).PropertyType;
                default:
                    throw new ArgumentException("MemberInfo must be if type FieldInfo or PropertyInfo", "member");
            }
        }
    }
}
