﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class Datos
    {
        [MapFieldFromDataSet(field: "CadenaOriginal", defaultValue: "")]
        public string CadenaOriginal { get; set; }
    }
}
