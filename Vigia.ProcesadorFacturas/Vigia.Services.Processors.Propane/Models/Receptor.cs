﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class Receptor
    {
        [MapFieldFromDataSet(field: "UsoCFDI", defaultValue: "")]
        public string UsoCfdi { get; internal set; }

        [MapFieldFromDataSet(field: "rfc", defaultValue: "")]
        public string Rfc { get; internal set; }

        [MapFieldFromDataSet(field: "nombre", defaultValue: "")]
        public string Nombre { get; internal set; }
    }
}
