﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Propane.Models;
using Vigia.Services.Processors.Propane.Services;

namespace Vigia.Services.Processors.Propane.Components
{
    internal class MailSenderComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IFacturasService facturasService;

        public MailSenderComponent(ILogFactory logFactory, IFacturasService facturasService)
        {
            logger = logFactory.Create(typeof(MailSenderComponent));
            this.facturasService = facturasService;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Cfdi[] cfdis = (Cfdi[])parameters[1];
            IList<Cfdi> successfulCfdis = new List<Cfdi>();

            foreach (Cfdi cfdi in cfdis)
            {
                try
                {
                    logger.Info($"Enviando email para factura {cfdi.Factura.SerieFolio} a los siguientes emails -> {cfdi.Factura.Email}...");
                    facturasService.SendCfdiByEmail(cfdi);
                    logger.Info($"Se envio email para factura {cfdi.Factura.SerieFolio}.");

                    successfulCfdis.Add(cfdi);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error enviando email para factura {cfdi.Factura.SerieFolio}. Mensaje: {ex.Message}");
                }
            }


            return successfulCfdis.ToArray();
        }
    }
}
