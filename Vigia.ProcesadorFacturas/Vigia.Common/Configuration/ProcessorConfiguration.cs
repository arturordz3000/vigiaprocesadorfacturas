﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vigia.Common.Attributes;

namespace Vigia.Services.Common.Configuration
{
    public class ProcessorConfiguration
    {
        [MapFieldFromDataSet("ZI_EMP")]
        public string CompanyId { get; set; }

        [MapFieldFromDataSet("ZI_INTER")]
        public int ExecutionIntervalInSeconds { get; set; }

        [MapFieldFromDataSet("ZI_RTDLL")]
        public string DllPath { get; set; }

        [MapFieldFromDataSet("ZI_RTXMLFA")]
        public string XmlPath { get; set; }

        [MapFieldFromDataSet("ZI_RTPDF")]
        public string PdfPath { get; set; }

        [MapFieldFromDataSet("ZI_RTPDF2")]
        public string Pdf2Path { get; set; }

        [MapFieldFromDataSet("ZI_RTQRFA")]
        public string QrCodesPath { get; set; }

        [MapFieldFromDataSet("ZI_RPTFCN")]
        public string RptFacturaContado { get; set; }

        [MapFieldFromDataSet("ZI_RPTFCR")]
        public string RptFacturaCredito { get; set; }

        [MapFieldFromDataSet("ZI_RPTFCR2")]
        public string RptFacturaCredito2 { get; set; }

        [MapFieldFromDataSet("ZI_RTREM")]
        public string RemisionPath { get; set; }

        [MapFieldFromDataSet("ZI_RTTICKE")]
        public string TicketsPath { get; set; }

        [MapFieldFromDataSet("ZI_EMFROM")]
        public string Email_From { get; set; }

        [MapFieldFromDataSet("ZI_EMSEND")]
        public string Email_SendMailCopyTo { get; set; }

        [MapFieldFromDataSet("ZI_EMSMTP")]
        public string Email_SmtpServer { get; set; }

        [MapFieldFromDataSet("ZI_EMPORT")]
        public int Email_Port { get; set; }

        [MapFieldFromDataSet("ZI_EMUSER")]
        public string Email_User { get; set; }

        [MapFieldFromDataSet("ZI_EMPASS")]
        public string Email_Password { get; set; }

        [MapFieldFromDataSet("ZI_ENVREM")]
        public bool Send_Remisiones { get; set; }

        [MapFieldFromDataSet("ZI_ENVTICK")]
        public bool Send_Tickets { get; set; }

        public DateTime? LastExecuteDateTime { get; set; }

        public static ProcessorConfiguration CreateFromJsonFile(string jsonFile)
        {
            string json = File.ReadAllText(jsonFile);

            return JsonConvert.DeserializeObject<ProcessorConfiguration>(json);
        }
    }

    internal enum State
    {
        Disabled,
        Enabled
    }
}
