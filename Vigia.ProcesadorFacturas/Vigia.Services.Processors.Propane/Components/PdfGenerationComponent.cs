﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vigia.Common.Components;
using Vigia.Common.Extensions;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Propane.Models;
using Vigia.Services.Processors.Propane.Services;

namespace Vigia.Services.Processors.Propane.Components
{
    internal class PdfGenerationComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IReportsService reportsService;

        public PdfGenerationComponent(ILogFactory logFactory, IReportsService reportsService)
        {
            logger = logFactory.Create(typeof(PdfGenerationComponent));
            this.reportsService = reportsService;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Cfdi[] cfdis = (Cfdi[])parameters[1];
            IList<Cfdi> successfulCfdis = new List<Cfdi>();

            foreach (Cfdi cfdi in cfdis)
            {
                try
                {
                    logger.Info($"Generando reportes para factura {cfdi.Factura.SerieFolio}...");
                    reportsService.GenerateReports(sessionId, cfdi);
                    logger.Info($"Reportes para factura {cfdi.Factura.SerieFolio} generados correctamente.");

                    successfulCfdis.Add(cfdi);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error generando reportes para factura {cfdi.Factura.SerieFolio}. Mensaje: {ex.GetInnerMessagesRecursively()}.\nStackTrace: " + ex.StackTrace);
                }
            }

            return successfulCfdis.ToArray();
        }
    }
}
