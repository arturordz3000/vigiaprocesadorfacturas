﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vigia.Common.Components;
using Vigia.Common.Factories;
using Vigia.Common.Logging;
using Vigia.Services.Processors.Mdi.Adapters;
using Vigia.Services.Processors.Mdi.Models;
using Vigia.Services.Processors.Mdi.Services;

namespace Vigia.Services.Processors.Mdi.Components
{
    internal class FacturaElectronicaInsertionComponent : IComponent
    {
        private readonly ILog logger;
        private readonly IFacturasService facturasService;

        public FacturaElectronicaInsertionComponent(ILogFactory logFactory, IFacturasService facturasService)
        {
            logger = logFactory.Create(typeof(FacturaElectronicaInsertionComponent));
            this.facturasService = facturasService;
        }

        public object Execute(params object[] parameters)
        {
            Guid sessionId = (Guid)parameters[0];
            Cfdi[] cfdis = (Cfdi[])parameters[1];
            IList<Cfdi> successfulCfdis = new List<Cfdi>();

            foreach (Cfdi cfdi in cfdis)
            {
                try
                {
                    logger.Info($"Insertando Factura Electronica en SZJ060_MDI para factura {cfdi.Factura.SerieFolio}...", sessionId);

                    facturasService.InsertFacturaElectronica(cfdi, new InsertFacturaElectronicaProcedureParametersAdapter(sessionId));

                    logger.Info($"Factura Electronica con Id: {cfdi.IdCfdi} insertada para factura {cfdi.Factura.SerieFolio} exitosamente", sessionId);

                    successfulCfdis.Add(cfdi);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error insertando Factura Electronica en SZJ060_MDI con Id: {cfdi.IdCfdi} para factura {cfdi.Factura.SerieFolio}. Mensaje: {ex.Message}", sessionId);
                }
            }

            return successfulCfdis.ToArray();
        }
    }
}
