﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vigia.ProcesadorFacturas.Service.Configuration
{
    internal static class AppSettings
    {
        public static readonly int TimerIntervalInSeconds = int.Parse(ConfigurationManager.AppSettings["Intervalo"]);
        public static readonly string DatabaseConnectionString = ConfigurationManager.ConnectionStrings["ProcessorsConfigurationDB"].ToString();
        public static readonly string ProcessorsConfigurationTableName = ConfigurationManager.AppSettings["ProcessorsConfigurationTableName"];
        public static readonly string ProcessorAppPath = ConfigurationManager.AppSettings["ProcessorAppPath"];
        public static readonly string ProcessorAppFolder = ConfigurationManager.AppSettings["ProcessorAppFolder"];
    }
}
