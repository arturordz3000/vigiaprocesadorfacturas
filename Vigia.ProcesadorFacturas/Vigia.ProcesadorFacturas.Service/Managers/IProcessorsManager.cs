﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vigia.Services.Common.Configuration;

namespace Vigia.ProcesadorFacturas.Service.Managers
{
    internal interface IProcessorsManager
    {
        void Start(ProcessorConfiguration[] processorsConfiguration);
        void UpdateConfiguration(ProcessorConfiguration[] processorsConfiguration);
        void StopAndWait();
    }
}
