﻿using System;
using System.Data;

namespace Vigia.Common.Attributes
{
    public class MapFieldFromDataSetAttribute : MapFieldFromDataSetBaseAttribute
    {
        private readonly string tableName;
        private readonly int tableIndex;
        private readonly bool isTableNameSet;
        private readonly string field;
        private readonly object defaultValue;

        public MapFieldFromDataSetAttribute(int tableIndex, string field, object defaultValue = null)
        {
            this.tableIndex = tableIndex;
            this.field = field;
            this.defaultValue = defaultValue;
            isTableNameSet = false;
        }

        public MapFieldFromDataSetAttribute(string tableName, string field, object defaultValue = null)
        {
            this.tableName = tableName;
            this.field = field;
            this.defaultValue = defaultValue;
            isTableNameSet = true;
        }

        public MapFieldFromDataSetAttribute(string field, object defaultValue = null)
        {
            this.field = field;
            this.defaultValue = defaultValue;
        }

        public override object GetMappedValue(string tableName, DataSet dataSet, int rowIndex = 0)
        {
            try
            {
                return dataSet.Tables[tableName].Rows[rowIndex][field];
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        public override object GetMappedValue(int tableIndex, DataSet dataSet, int rowIndex)
        {
            try
            {
                return dataSet.Tables[tableIndex].Rows[rowIndex][field];
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        public override object GetMappedValue(DataSet dataSet, int rowIndex = 0)
        {
            try
            {
                if (isTableNameSet)
                {
                    if (string.IsNullOrEmpty(tableName))
                        throw new Exception($"{nameof(tableName)} must be defined.");

                    return dataSet.Tables[tableName].Rows[rowIndex][field];
                }
                else
                    return dataSet.Tables[tableIndex].Rows[rowIndex][field];
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
    }
}
