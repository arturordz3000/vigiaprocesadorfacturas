﻿using System.Composition;
using System.Threading;
using Ninject;
using Vigia.Common.Configuration;
using Vigia.Common.Factories;
using Vigia.Common.Pipelining;
using Vigia.Common.Processing;
using Vigia.Services.Processors.Mdi.Injection;

namespace Vigia.Services.Processors.Mdi
{
    [Export(typeof(IProcessor))]
    public class MainProcessor : IProcessor
    {
        private bool isRunning = true;
        private readonly IKernel kernel = new StandardKernel(new Bindings());

        public void Run(bool isSingleMode = false)
        {
            IPipelineFactory pipelineFactory = kernel.Get<IPipelineFactory>();
            IPipeline pipeline = pipelineFactory.Create();

            if (isSingleMode)
            {
                pipeline.Execute();
                return;
            }

            while (isRunning)
            {
                pipeline.Execute();
                Thread.Sleep(ApplicationConfiguration.GetInstance().ProcessingInterval);
            }
        }

        public void Stop()
        {
            isRunning = false;
        }
    }
}
