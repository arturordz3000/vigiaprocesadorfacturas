﻿using Newtonsoft.Json;
using Ninject;
using System;
using Vigia.Common.Configuration;
using Vigia.Common.Extensions;
using Vigia.Common.Factories;
using Vigia.Common.Loaders;
using Vigia.Common.Logging;
using Vigia.Common.Mapping;
using Vigia.Common.Processing;
using Vigia.ProcesadorFacturas.Injection;
using Vigia.Services.Common.Configuration;

namespace Vigia.ProcesadorFacturas
{
    class Program
    {
        private static readonly IKernel kernel = new StandardKernel(new Bindings());
        private static ILog log;

        static void Main(string[] args)
        {
            log = kernel.Get<ILogFactory>().Create(typeof(Program));

            if (args.Length > 0 && args[0] == "--singleMode")
            {
                RunSingleMode(args);
                return;
            }

            RunDefaultMode();
        }

        private static void RunDefaultMode()
        {
            ILoader<IProcessor[]> processorsLoader = kernel.Get<ILoader<IProcessor[]>>();
            IProcessor[] processors = processorsLoader.Load();

            processors.RunAndWait();
        }

        private static void RunSingleMode(string[] args)
        {
            ProcessorConfiguration configuration = null;

            if (args.Length < 2)
            {
                log.Error("No se incluyó el argumento 'paramsFile'. Este argumento es una ruta a un archivo JSON, el cual se necesita para inicializar el procesador de facturas.");
                return;
            }

            try
            {
                string paramsFile = args[1];
                configuration = ProcessorConfiguration.CreateFromJsonFile(paramsFile);
                ApplicationConfiguration.SetSingleton(new ProcessorsConfigurationToApplicationConfigurationEnvelope(configuration));

                ILoader<IProcessor[]> processorsLoader = kernel.Get<ILoader<IProcessor[]>>();
                IProcessor[] processors = processorsLoader.Load();

                processors.RunAndWait(true);
            }
            catch (Exception ex)
            {
                log.Error("Ha ocurrido un error inesperado al inicializar Single Mode. Error: " + ex.GetInnerMessagesRecursively() + "Stacktrace:\n" + ex.StackTrace + "\nConfiguration: " + JsonConvert.SerializeObject(configuration));
            }
        }
    }
}
