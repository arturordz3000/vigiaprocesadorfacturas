﻿using System;
using System.Data;

namespace Vigia.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public abstract class MapTableFromDataSetBaseAttribute : Attribute
    {
        public abstract void MapAllFields(object currentObject, DataSet dataSet, int rowIndex = 0);
    }
}
