﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Vigia.Common.Database;
using Vigia.Services.Processors.Mdi.Models;

namespace Vigia.Services.Processors.Mdi.Adapters
{
    internal class InsertCfdiDetailsForVersion3ProcedureParametersAdapter : IProcedureParametersAdapter<CfdiDetail>
    {
        internal enum Case
        {
            Version3,
            AnyVersion
        }

        private readonly Guid session;
        private readonly int cfdiId;

        public InsertCfdiDetailsForVersion3ProcedureParametersAdapter(Guid session, int cfdiId)
        {
            this.session = session;
            this.cfdiId = cfdiId;
        }

        public object[] Adapt(CfdiDetail toAdapt)
        {
            IList<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@factura_id", cfdiId));
            parameters.Add(new SqlParameter("@sesion", session));
            parameters.Add(new SqlParameter("@detalle_id", toAdapt.CfdiDetailId));
            parameters.Add(new SqlParameter("@claveprodserv", toAdapt.DetailDataRow[1]));
            parameters.Add(new SqlParameter("@claveunidad", toAdapt.DetailDataRow[3]));


            return parameters.ToArray();
        }
    }
}
