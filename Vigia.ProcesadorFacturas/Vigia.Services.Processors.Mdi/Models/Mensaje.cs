﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class Mensaje
    {
        [MapFieldFromDataSet(field: "msj", defaultValue: "")]
        public string Msj { get; set; }
    }
}
