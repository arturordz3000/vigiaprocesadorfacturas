﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class Datos
    {
        [MapFieldFromDataSet(field: "CadenaOriginal", defaultValue: "")]
        public string CadenaOriginal { get; set; }
    }
}
