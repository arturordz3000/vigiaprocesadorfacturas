﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class RegimenFiscal
    {
        [MapFieldFromDataSet(field: "Regimen", defaultValue: "")]
        public string Regimen { get; internal set; }
    }
}
