﻿using System.Runtime.CompilerServices;
using Vigia.Common.Attributes;

[assembly: InternalsVisibleTo("Vigia.Tests")]

namespace Vigia.Services.Processors.Mdi.Models
{
    internal class Cfdi
    {
        public Factura Factura { get; set; }

        public int IdCfdi { get; set; }

        [MapTableFromDataSet("Comprobante")]
        public Comprobante Comprobante { get; set; }

        [MapTableFromDataSet("Emisor")]
        public Emisor Emisor { get; set; }

        [MapTableFromDataSet("DomicilioFiscal")]
        public Domicilio DomicilioFiscal { get; set; }

        [MapTableFromDataSet("ExpedidoEn")]
        public Domicilio ExpedidoEn { get; set; }

        [MapTableFromDataSet("RegimenFiscal")]
        public RegimenFiscal RegimenFiscal { get; set; }

        [MapTableFromDataSet("Receptor")]
        public Receptor Receptor { get; set; }

        [MapTableFromDataSet("Domicilio")]
        public Domicilio Domicilio { get; set; }

        [MapTableFromDataSet("Impuestos")]
        public Impuestos Impuestos { get; set; }

        [MapTableFromDataSet("TimbreFiscalDigital")]
        public TimbreFiscalDigital TimbreFiscalDigital { get; set; }

        [MapTableFromDataSet("Datos")]
        public Datos Datos { get; set; }

        [MapTableFromDataSet("Mensaje")]
        public Mensaje[] Mensaje { get; set; }

        public bool IsVersion33 { get { return Comprobante != null ? Comprobante.Version == "3.3" : false; } }
    }
}
