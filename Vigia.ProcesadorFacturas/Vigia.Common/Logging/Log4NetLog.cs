﻿using System;
using log4net;

namespace Vigia.Common.Logging
{
    public class Log4NetLog : ILog
    {
        private readonly log4net.ILog log;

        public Log4NetLog(Type type)
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(type);
        }

        public void Debug(string message)
        {
            log.Debug(message);
        }

        public void Debug(string message, Guid sessionId)
        {
            Debug(AppendSessionId(message, sessionId));
        }

        public void Error(string message)
        {
            log.Error(message);
        }

        public void Error(string message, Guid sessionId)
        {
            Error(AppendSessionId(message, sessionId));
        }

        public void Info(string message)
        {
            log.Info(message);
        }

        public void Info(string message, Guid sessionId)
        {
            Info(AppendSessionId(message, sessionId));
        }

        public void Warning(string message)
        {
            log.Warn(message);
        }

        public void Warning(string message, Guid sessionId)
        {
            Warning(AppendSessionId(message, sessionId));
        }

        private string AppendSessionId(string message, Guid sessionId)
        {
            return $"SessionId: {sessionId} -> {message}";
        }
    }
}
