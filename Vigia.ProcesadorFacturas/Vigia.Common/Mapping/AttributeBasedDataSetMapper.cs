﻿using System;
using System.Data;
using System.Reflection;
using Vigia.Common.Attributes;
using Vigia.Common.Extensions;

namespace Vigia.Common.Mapping
{
    public class AttributeBasedDataSetMapper : IDataSetMapper
    {
        public T MapTo<T>(DataSet dataSet)
        {
            Type type = typeof(T);

            return type.IsArray ? (T)MapArrayRecursively(typeof(T), dataSet) : (T)MapRecursively(typeof(T), dataSet);
        }

        private object MapArrayRecursively(Type type, DataSet dataSet)
        {
            int dataSetRowsCount = dataSet.Tables[0].Rows.Count;
            object[] currentObject = (object[])Activator.CreateInstance(type, dataSetRowsCount);

            for (int i = 0; i < dataSetRowsCount; i++)
            {
                var mappedObject = MapRecursively(type.GetElementType(), dataSet, null, i);
                currentObject[i] = mappedObject;
            }

            return currentObject;
        }

        private object MapRecursively(Type type, DataSet dataSet, string tableName = null, int rowIndex = 0)
        {
            if (type.BaseType == typeof(Array))
                return MapRecursivelyArray(type, dataSet, tableName);

            return MapRecursivelySingle(type, dataSet, tableName, rowIndex);
        }

        private object MapRecursivelyArray(Type type, DataSet dataSet, string tableName)
        {
            if (!dataSet.Tables.Contains(tableName))
                return null;

            int rowCount = dataSet.Tables[tableName].Rows.Count;
            Type elementType = type.GetElementType();
            Array array = Array.CreateInstance(elementType, rowCount);

            for (int i = 0; i < rowCount; i++)
                array.SetValue(MapRecursivelySingle(elementType, dataSet, tableName, i), i);

            return array;
        }

        private object MapRecursivelySingle(Type type, DataSet dataSet, string tableName = null, int rowIndex = 0)
        {
            object currentObject = Activator.CreateInstance(type);

            MemberInfo[] members = currentObject.GetType().GetMembers();

            foreach (MemberInfo memberInfo in members)
            {
                MapTableFromDataSetBaseAttribute tableMappingAttribute = memberInfo.GetCustomAttribute<MapTableFromDataSetBaseAttribute>();

                if (tableMappingAttribute != null)
                {
                    MapTable(currentObject, tableMappingAttribute, memberInfo, dataSet, rowIndex);
                    continue;
                }

                MapFieldFromDataSetBaseAttribute fieldMappingAttribute = memberInfo.GetCustomAttribute<MapFieldFromDataSetBaseAttribute>();

                if (fieldMappingAttribute != null)
                {
                    if (string.IsNullOrEmpty(tableName))
                        memberInfo.SetValue(currentObject, fieldMappingAttribute.GetMappedValue(dataSet, rowIndex));
                    else
                        memberInfo.SetValue(currentObject, fieldMappingAttribute.GetMappedValue(tableName, dataSet, rowIndex));
                }
            }

            return currentObject;
        }

        private void MapTable(object currentObject, MapTableFromDataSetBaseAttribute tableMappingAttribute, MemberInfo memberInfo, DataSet dataSet, int rowIndex = 0)
        {
            string tableName = ((MapTableFromDataSetAttribute)tableMappingAttribute).tableName;

            tableMappingAttribute.MapAllFields(currentObject, dataSet, rowIndex);

            if (memberInfo.IsVariable() && memberInfo.GetVariableType().IsCustomType())
            {
                object recursiveObject = MapRecursively(memberInfo.GetVariableType(), dataSet, tableName);
                memberInfo.SetValue(currentObject, recursiveObject);
            }
        }

        private bool IsCustomType(Type type)
        {
            return type.Assembly == Assembly.GetExecutingAssembly();
        }
    }
}
