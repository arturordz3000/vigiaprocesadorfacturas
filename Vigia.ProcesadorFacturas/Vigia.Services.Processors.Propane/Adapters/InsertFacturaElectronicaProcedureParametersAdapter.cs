﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Vigia.Common.Database;
using Vigia.Services.Processors.Propane.Models;

namespace Vigia.Services.Processors.Propane.Adapters
{
    internal class InsertFacturaElectronicaProcedureParametersAdapter : IProcedureParametersAdapter<Cfdi>
    {
        private readonly Guid session;

        public InsertFacturaElectronicaProcedureParametersAdapter(Guid session)
        {
            this.session = session;
        }

        public object[] Adapt(Cfdi toAdapt)
        {
            IList<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ZJ_FILIAL", toAdapt.Factura.E1_Filial));
            parameters.Add(new SqlParameter("@ZJ_SERIE", toAdapt.Factura.Serie));
            parameters.Add(new SqlParameter("@ZJ_DOC", toAdapt.Factura.Folio));
            parameters.Add(new SqlParameter("@ZJ_CLIENTE", toAdapt.Factura.E1_Cliente));
            parameters.Add(new SqlParameter("@ZJ_LOJA", toAdapt.Factura.E1_Loja));
            parameters.Add(new SqlParameter("@ZJ_IMPORTE", toAdapt.Factura.E1_Valor));
            parameters.Add(new SqlParameter("@ZJ_TIPO", toAdapt.Factura.E1_Tipo));

            return parameters.ToArray();
        }
    }
}
