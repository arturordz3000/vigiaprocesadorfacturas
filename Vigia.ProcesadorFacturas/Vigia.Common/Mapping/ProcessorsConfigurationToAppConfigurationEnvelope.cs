﻿using System;
using Vigia.Common.Configuration;
using Vigia.Services.Common.Configuration;

namespace Vigia.Common.Mapping
{
    public class ProcessorsConfigurationToApplicationConfigurationEnvelope : IEnvelope<ApplicationConfiguration>
    {
        private ProcessorConfiguration configuration;

        public ProcessorsConfigurationToApplicationConfigurationEnvelope(ProcessorConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public ApplicationConfiguration Unwrap()
        {
            ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration();

            applicationConfiguration.CompanyId = configuration.CompanyId;
            applicationConfiguration.ProcessorsBasePath = configuration.DllPath;
            applicationConfiguration.XmlFilesDirectoryPath = configuration.XmlPath;
            applicationConfiguration.QRCodesDirectoryPath = configuration.QrCodesPath;
            applicationConfiguration.RptFacturaContado = configuration.RptFacturaContado;
            applicationConfiguration.RptFacturaCredito = configuration.RptFacturaCredito;
            applicationConfiguration.RptFacturaCreditoV2 = configuration.RptFacturaCredito2;
            applicationConfiguration.PdfDirectoryPath = configuration.PdfPath;
            applicationConfiguration.PdfDirectoryPath2 = configuration.Pdf2Path;
            applicationConfiguration.RemisionDirectoryPath = configuration.RemisionPath;
            applicationConfiguration.TicketsDirectoryPath = configuration.TicketsPath;
            applicationConfiguration.MailFrom = configuration.Email_From;
            applicationConfiguration.SendMailCopyTo = configuration.Email_SendMailCopyTo;
            applicationConfiguration.SmtpServer = configuration.Email_SmtpServer;
            applicationConfiguration.Port = configuration.Email_Port;
            applicationConfiguration.User = configuration.Email_User;
            applicationConfiguration.Password = configuration.Email_Password;
            applicationConfiguration.SendRemisiones = configuration.Send_Remisiones;
            applicationConfiguration.SendTickets = configuration.Send_Tickets;

            return applicationConfiguration;
        }
    }
}
