﻿using System.Configuration;
using Vigia.Common.Mapping;

namespace Vigia.Common.Configuration
{
    public class ApplicationConfiguration
    {
        private static ApplicationConfiguration applicationConfiguration;

        public static void SetSingleton(IEnvelope<ApplicationConfiguration> envelope)
        {
            applicationConfiguration = envelope.Unwrap();
        }

        public static ApplicationConfiguration GetInstance()
        {
            if (applicationConfiguration == null)
                applicationConfiguration = new ApplicationConfiguration();

            return applicationConfiguration;
        }

        public string ProcessorsBasePath { get; set; } = ConfigurationManager.AppSettings["ProcessorsBasePath"].ToString();
        public int ProcessingInterval { get; set; } = int.Parse(ConfigurationManager.AppSettings["Intervalo"]);
        public string TermogasDatabase { get; set; } = ConfigurationManager.ConnectionStrings["TermogasDB"].ToString();
        public string CompanyId { get; set; } = "06";
        public string XmlFilesDirectoryPath { get; set; } = ConfigurationManager.AppSettings["RutaXML"].ToString();
        public string QRCodesDirectoryPath { get; set; } = ConfigurationManager.AppSettings["RutaQRCodes"].ToString();
        public string RptFacturaContado { get; set; } = ConfigurationManager.AppSettings["rptFacturaContado"].ToString();
        public string RptFacturaCredito { get; set; } = ConfigurationManager.AppSettings["rptFacturaCredito"].ToString();
        public string RptFacturaCreditoV2 { get; set; } = ConfigurationManager.AppSettings["rptFacturaCreditoV2"].ToString();
        public object PdfDirectoryPath { get; set; } = ConfigurationManager.AppSettings["RutaPDF"].ToString();
        public object PdfDirectoryPath2 { get; set; } = ConfigurationManager.AppSettings["RutaPDF2"].ToString();
        public string RemisionDirectoryPath { get; set; } = ConfigurationManager.AppSettings["rutaRemision"].ToString();
        public string TicketsDirectoryPath { get; set; } = ConfigurationManager.AppSettings["rutaTickets"].ToString();
        public string MailFrom { get; set; } = ConfigurationManager.AppSettings["From"].ToString();
        public string SendMailCopyTo { get; set; } = ConfigurationManager.AppSettings["SendMailCopyTo"].ToString();
        public string SmtpServer { get; set; } = ConfigurationManager.AppSettings["SMTPServer"].ToString();
        public int Port { get; set; } = int.Parse(ConfigurationManager.AppSettings["Port"]);
        public string User { get; set; } = ConfigurationManager.AppSettings["Usuario"].ToString();
        public string Password { get; set; } = ConfigurationManager.AppSettings["Password"].ToString();
        public bool SendRemisiones { get; set; } = true;
        public bool SendTickets { get; set; } = true;
    }
}
