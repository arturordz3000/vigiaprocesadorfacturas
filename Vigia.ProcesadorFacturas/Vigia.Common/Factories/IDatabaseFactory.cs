﻿using Vigia.Common.Database;

namespace Vigia.Common.Factories
{
    public interface IDatabaseFactory
    {
        IDatabase Create(string connectionString);
    }
}
