﻿using System;

namespace Vigia.Common.Extensions
{
    public static class ExceptionExceptions
    {
        public static string GetInnerMessagesRecursively(this Exception ex)
        {
            return GetInnerMessagesRecursively(ex.InnerException, ex.Message);
        }

        private static string GetInnerMessagesRecursively(Exception ex, string previousMessages)
        {
            if (ex == null)
                return previousMessages;

            return GetInnerMessagesRecursively(ex.InnerException, previousMessages + "\nInnerExceptionMessage: " + ex.Message);
        }
    }
}
