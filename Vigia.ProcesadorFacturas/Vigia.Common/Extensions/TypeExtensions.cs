﻿using System;

namespace Vigia.Common.Extensions
{
    public static class TypeExtensions
    {
        public static bool IsCustomType(this Type type)
        {
            return type.Namespace.Contains("Vigia");
        }
    }
}
