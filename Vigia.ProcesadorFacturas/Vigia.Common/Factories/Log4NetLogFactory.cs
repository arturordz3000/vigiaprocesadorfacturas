﻿using System;
using Vigia.Common.Logging;

namespace Vigia.Common.Factories
{
    public class Log4NetLogFactory : ILogFactory
    {
        public ILog Create(Type type)
        {
            return new Log4NetLog(type);
        }
    }
}
