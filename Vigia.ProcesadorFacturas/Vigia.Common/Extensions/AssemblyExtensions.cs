﻿using System.Composition.Hosting;
using System.Reflection;
using Vigia.Common.Processing;

namespace Vigia.Common.Extensions
{
    public static class AssemblyExtensions
    {
        public static IProcessor GetProcessor(this Assembly assembly)
        {
            var configuration = new ContainerConfiguration().WithAssembly(assembly);
            using (var container = configuration.CreateContainer())
            {
                return container.GetExport<IProcessor>();
            }
        }
    }
}
