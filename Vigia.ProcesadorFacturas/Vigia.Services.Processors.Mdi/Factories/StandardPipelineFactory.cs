﻿using Vigia.Common.Factories;
using Vigia.Common.Mapping;
using Vigia.Common.Pipelining;
using Vigia.Services.Processors.Mdi.Pipelining;
using Vigia.Services.Processors.Mdi.Services;

namespace Vigia.Services.Processors.Mdi.Factories
{
    internal class StandardPipelineFactory : IPipelineFactory
    {
        private readonly ILogFactory logFactory;
        private readonly IFacturasService facturasService;
        private readonly IDataSetMapper dataSetMapper;
        private readonly IReportsService reportsService;

        public StandardPipelineFactory(ILogFactory logFactory, IFacturasService facturasService, IDataSetMapper dataSetMapper, IReportsService reportsService)
        {
            this.logFactory = logFactory;
            this.facturasService = facturasService;
            this.dataSetMapper = dataSetMapper;
            this.reportsService = reportsService;
        }

        public IPipeline Create()
        {
            return new StandardPipeline(logFactory, facturasService, dataSetMapper, reportsService);
        }
    }
}
