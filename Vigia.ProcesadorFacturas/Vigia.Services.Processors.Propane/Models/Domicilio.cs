﻿using Vigia.Common.Attributes;

namespace Vigia.Services.Processors.Propane.Models
{
    internal class Domicilio
    {
        [MapFieldFromDataSet(field: "calle", defaultValue: "")]
        public string Calle { get; set; }

        [MapFieldFromDataSet(field: "noExterior", defaultValue: "")]
        public string NoExterior { get; set; }

        [MapFieldFromDataSet(field: "colonia", defaultValue: "")]
        public string Colonia { get; set; }

        [MapFieldFromDataSet(field: "municipio", defaultValue: "")]
        public string Municipio { get; set; }

        [MapFieldFromDataSet(field: "estado", defaultValue: "")]
        public string Estado { get; set; }

        [MapFieldFromDataSet(field: "pais", defaultValue: "")]
        public string Pais { get; set; }

        [MapFieldFromDataSet(field: "codigoPostal", defaultValue: "")]
        public string CodigoPostal { get; set; }
    }
}
