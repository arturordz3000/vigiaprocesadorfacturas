﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using Vigia.Common.Mapping;
using Vigia.Common.Utils;

namespace Vigia.Tests
{
    [TestClass]
    public class CfdiDeserializationTests
    {
        [TestMethod]
        public void Map_Cfdi_With_Factura_Propane_Returns_Cfdi()
        {
            AttributeBasedDataSetMapper mapper = new AttributeBasedDataSetMapper();
            DataSet loadedDataSet = XmlUtils.LoadXmlAsDataSet("TestData/factura_propane.xml");

            var cfdi = mapper.MapTo<Services.Processors.Propane.Models.Cfdi>(loadedDataSet);

            AssertPropaneCfdi(cfdi);
        }

        private void AssertPropaneCfdi(Services.Processors.Propane.Models.Cfdi cfdi)
        {
            Assert.IsNotNull(cfdi);
            Assert.IsNotNull(cfdi.Comprobante);
            Assert.IsNotNull(cfdi.Datos);
            Assert.IsNotNull(cfdi.Domicilio);
            Assert.IsNotNull(cfdi.DomicilioFiscal);
            Assert.IsNotNull(cfdi.Emisor);
            Assert.IsNotNull(cfdi.ExpedidoEn);
            Assert.IsNotNull(cfdi.Impuestos);
            Assert.IsNotNull(cfdi.Mensaje);
            Assert.IsNotNull(cfdi.Receptor);
            Assert.IsNotNull(cfdi.RegimenFiscal);
            Assert.IsNotNull(cfdi.TimbreFiscalDigital);
        }

        [TestMethod]
        public void Map_Cfdi_With_Factura_Mdi_Returns_Cfdi()
        {
            AttributeBasedDataSetMapper mapper = new AttributeBasedDataSetMapper();
            DataSet loadedDataSet = XmlUtils.LoadXmlAsDataSet("TestData/factura_mdi.xml");

            var cfdi = mapper.MapTo<Services.Processors.Mdi.Models.Cfdi>(loadedDataSet);

            AssertMdiCfdi(cfdi);
        }

        private void AssertMdiCfdi(Services.Processors.Mdi.Models.Cfdi cfdi)
        {
            Assert.IsNotNull(cfdi);
            Assert.IsNotNull(cfdi.Comprobante);
            Assert.IsNotNull(cfdi.Datos);
            Assert.IsNotNull(cfdi.Domicilio);
            Assert.IsNotNull(cfdi.DomicilioFiscal);
            Assert.IsNotNull(cfdi.Emisor);
            Assert.IsNotNull(cfdi.ExpedidoEn);
            Assert.IsNotNull(cfdi.Impuestos);
            Assert.IsNull(cfdi.Mensaje);
            Assert.IsNotNull(cfdi.Receptor);
            Assert.IsNotNull(cfdi.RegimenFiscal);
            Assert.IsNotNull(cfdi.TimbreFiscalDigital);
        }
    }
}
