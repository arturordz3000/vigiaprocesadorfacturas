﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vigia.Common.Extensions;

namespace Vigia.Tests
{
    [TestClass]
    public class NumberExtensionsTest
    {
        [TestMethod]
        public void GetCurrencyDescription_Returns_CurrencyDescription()
        {
            double number = 1000;
            Assert.AreEqual("MIL PESOS 0/100 M.N.", NumberExtensions.GetCurrencyDescription(number));
            number = 1500;
            Assert.AreEqual("MIL QUINIENTOS PESOS 0/100 M.N.", NumberExtensions.GetCurrencyDescription(number));
            number = 1751.65;
            Assert.AreEqual("MIL SETECIENTOS CINCUENTA Y UN PESOS 65/100 M.N.", NumberExtensions.GetCurrencyDescription(number));
            number = 1351782.56;
            Assert.AreEqual("UN MILLON TRESCIENTOS CINCUENTA Y UN MIL SETECIENTOS OCHENTA Y DOS PESOS 56/100 M.N.", NumberExtensions.GetCurrencyDescription(number));
        }
    }
}
